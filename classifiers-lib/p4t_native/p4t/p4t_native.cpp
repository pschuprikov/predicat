#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <optional>
#include <unordered_set>

#include <p4t/model/filter.h>
#include <p4t/model/support.hpp>
#include <p4t/utils/python_utils.h>

#include <p4t/opt/boolean_minimization.h>
#include <p4t/opt/chain_algos.h>
#include <p4t/opt/distribution_algos.h>
#include <p4t/opt/expansion_algos.h>
#include <p4t/opt/oi_algos.h>
#include <p4t/opt/oi_lpm_algos.h>
#include <p4t/opt/updates.h>
#include <p4t/transformations/transformations.h>

#include "p4t_native.h"

namespace {

using namespace p4t;
using namespace p4t::model;
using namespace p4t::utils;
namespace py = p4t::py;

auto map_partition_indices(
        vector<vector<Support>> const &partition,
        vector<Support> const &supports) -> vector<vector<int>> {

    support_map<int> support2partition{};
    for (auto i = 0u; i < partition.size(); i++) {
        for (auto const &s : partition[i]) {
            support2partition[s] = i;
        }
    }

    vector<vector<int>> result(partition.size());
    for (auto i = 0u; i < supports.size(); i++) {
        if (support2partition.count(supports[i])) {
            result[support2partition[supports[i]]].emplace_back(i);
        }
    }

    return result;
}

template <class T>
auto optional_from_python(py::object obj) -> std::optional<T> {
    if (obj.is_none()) {
        return std::nullopt;
    }
    return py::extract<T>(obj);
}

auto bitrange_from_python(py::object bitrange)
        -> std::optional<std::tuple<int, int>> {
    if (bitrange.is_none()) {
        return std::nullopt;
    }
    return make_tuple<int, int>(
            py::extract<int>(bitrange.attr("start_bit")),
            py::extract<int>(bitrange.attr("end_bit")));
}

auto bitranges_from_python(py::object py_bit_ranges) {
    if (py_bit_ranges.is_none()) {
        return vector<std::tuple<int, int>>{};
    }
    vector<std::tuple<int, int>> bit_ranges(len(py_bit_ranges));
    for (auto i = 0; i < len(py_bit_ranges); i++) {
        bit_ranges[0] = bitrange_from_python(py_bit_ranges[i]).value();
    }
    return bit_ranges;
}

auto greedy_strategy_from_python(py::object py_strategy)
        -> std::optional<p4t::opt::GreedyStrategy> {
    if (py_strategy == py::object{}) {
        return std::nullopt;
    }

    auto const strategy = std::string(py::extract<std::string>(py_strategy));

    if (strategy == "max_lpm") {
        return p4t::opt::GreedyStrategy::MAX_LPM;
    }
    if (strategy == "max_oi") {
        return p4t::opt::GreedyStrategy::MAX_OI;
    }
    if (strategy == "max_step") {
        return p4t::opt::GreedyStrategy::MAX_STEP;
    }
    throw std::invalid_argument(
            fmt::format("unknown greedy strategy: {:s}", strategy));
}

auto oi_lpm_method_from_python(std::string method) -> p4t::opt::OiLpmMethod {
    if (method == "greedy_all") {
        return p4t::opt::OiLpmMethod::GREEDY_ALL;
    }
    if (method == "original_supports") {
        return p4t::opt::OiLpmMethod::ORIGINAL_SUPPORTS;
    }
    if (method == "incremental") {
        return p4t::opt::OiLpmMethod::INCREMENTAL;
    }
    throw std::invalid_argument(
            fmt::format("unknown oi_lpm_method: {:s}", method));
}

template <class F>
auto do_min_bmgr1_w_oi(
        vector<F> const &rules, bool reduce_width_before_lpm,
        std::optional<int> max_width,
        std::optional<p4t::opt::GreedyStrategy> strategy,
        p4t::opt::OiLpmMethod algo) {
    auto [transformation, chain] = opt::find_oi_lpm_subset(
            rules,
            {
                    .max_support_size = max_width,
                    .method = algo,
                    .greedy_strategy = strategy,
                    .width_reduction_order =
                            reduce_width_before_lpm
                                    ? opt::OiLpmWidthReductionOrder::BEFORE
                                    : opt::OiLpmWidthReductionOrder::AFTER,
            });
    return make_tuple(transformation, chain);
}
template <class F>
auto do_best_subgroup(
        vector<F> const &rules, opt::OIMode max_oi_mode, string algo, int l,
        bool only_exact, vector<tuple<int, int>> bit_ranges)
        -> optional<transformations::SubsetWithCommonMask> {
    // if (algo == "min_similarity") {
    //    auto const bits = opt::best_min_similarity_bits(rules, l);
    //    auto const result = opt::find_maximal_oi_subset(
    //            rules, opt::bits_to_mask(bits), max_oi_mode);
    //    return py::make_tuple(to_python(bits), to_python(result));
    //} else
    if (algo == "icnp_oi" || algo == "icnp_blockers") {
        auto const minme_mode = algo == "icnp_oi" ? opt::MinMEMode::MAX_OI
                                                  : opt::MinMEMode::BLOCKERS;
        auto opt_bit_ranges = [&]() -> vector<p4t::opt::BitRange> {
            if (bit_ranges.empty()) {
                return {{}};
            } else {
                auto opt_bit_ranges = vector<p4t::opt::BitRange>();
                std::transform(
                        begin(bit_ranges), end(bit_ranges),
                        std::back_inserter(opt_bit_ranges), [](auto x) {
                            return p4t::opt::BitRange{.upper = to_support(x)};
                        });
                return opt_bit_ranges;
            }
        }();
        log()->info("done getting bit ranges");

        return optional(opt::best_to_stay_minme(
                rules, {.l = static_cast<size_t>(l),
                        .mode = minme_mode,
                        .oi_mode = max_oi_mode,
                        .only_exact = only_exact,
                        .bit_ranges = opt_bit_ranges}));
    } else {
        return std::nullopt;
    }
};

} // namespace

auto p4t::min_pmgr(py::object svmr) -> py::object {
    if (py::len(svmr) == 0) {
        return py::object();
    }

    auto const filters = svmr2filters(svmr);

    auto const supports = to_supports(filters);
    auto const supports_unique = select_unique(supports);

    auto const partition = opt::find_min_chain_partition(supports_unique);
    auto const partition_indices = map_partition_indices(partition, supports);

    return py::make_tuple(to_python(partition), to_python(partition_indices));
}

auto p4t::min_bmgr1_w_oi(
        py::object svmr, bool use_actions, bool reduce_width_before_lpm,
        py::object py_max_width, string py_algo, py::object py_strategy)
        -> py::object {

    if (py::len(svmr) == 0) {
        return py::object();
    }

    auto const max_width = optional_from_python<int>(py_max_width);
    auto const strategy = greedy_strategy_from_python(py_strategy);
    auto const algo = oi_lpm_method_from_python(py_algo);
    auto [transformation, chain] =
            use_actions ? do_min_bmgr1_w_oi(
                                  svmr2filters(svmr), reduce_width_before_lpm,
                                  max_width, strategy, algo)
                        : do_min_bmgr1_w_oi(
                                  svmr2rules(svmr), reduce_width_before_lpm,
                                  max_width, strategy, algo);
    return py::make_tuple(
            to_python(transformation.indices()), to_python(chain.get_chain()));
}

auto p4t::min_bmgr(py::object svmrs, int max_num_groups) -> py::object {
    auto const n_supports = svmrs2supports(svmrs);

    vector<vector<Support>> n_unique_supports(n_supports.size());
    vector<vector<int>> n_weights(n_supports.size());
    for (auto i = 0u; i < n_supports.size(); ++i) {
        tie(n_unique_supports[i], n_weights[i]) =
                select_unique_n_weight(n_supports[i]);
    }

    auto const partitions = opt::find_min_bounded_chain_partition(
            n_unique_supports, n_weights, max_num_groups);

    vector<vector<vector<int>>> n_partition_indices(len(svmrs));
    for (auto i = 0; i < len(svmrs); i++) {
        n_partition_indices[i] =
                map_partition_indices(partitions[i], n_supports[i]);
    }

    return py::make_tuple(
            to_python(partitions), to_python(n_partition_indices));
}

auto p4t::best_subgroup(
        py::object svmr, int l, bool only_exact, bool use_actions, string algo,
        string max_oi_algo, py::object py_bit_ranges) -> py::object {
    auto const filters = svmr2filters(svmr);
    auto const max_oi_mode = max_oi_algo == "top_down"
                                     ? opt::OIMode::TOP_DOWN
                                     : opt::OIMode::MIN_DEGREE;
    auto const bit_ranges = bitranges_from_python(py_bit_ranges);
    auto const bits_n_result =
            use_actions ? do_best_subgroup(
                                  svmr2rules(svmr), max_oi_mode, algo, l,
                                  only_exact, bit_ranges)
                        : do_best_subgroup(
                                  svmr2filters(svmr), max_oi_mode, algo, l,
                                  only_exact, bit_ranges);

    if (!bits_n_result) {
        return py::object{};
    } else {
        return py::make_tuple(
                to_python(model::to_support(bits_n_result->mask())),
                to_python(bits_n_result->indices()));
    }
}

void p4t::set_num_threads([[maybe_unused]] int num_threads) {
    // omp_set_dynamic(false);
    // omp_set_num_threads(num_threads);
}

auto p4t::min_bmgr1_w_expansions(py::object classifier, int max_expanded_bits)
        -> py::object {
    if (len(classifier) == 0) {
        return py::object();
    }
    auto const supports = to_supports(svmr2filters(classifier));
    vector<vector<Support>> unique_supports(1);
    vector<vector<int>> weights(1);
    tie(unique_supports[0], weights[0]) = select_unique_n_weight(supports);

    auto const chain = opt::find_min_bounded_chain_partition(
            unique_supports, weights, 1)[0][0];

    auto [chain_w_expansions, expansions] = opt::try_expand_chain(
            chain, unique_supports[0], weights[0], max_expanded_bits);

    support_set in_chain(begin(chain_w_expansions), end(chain_w_expansions));
    vector<int> indices;
    vector<Support> exps;
    for (auto i = 0; i < int(supports.size()); i++) {
        if (in_chain.count(expansions[supports[i]])) {
            indices.push_back(i);
            exps.push_back(expansions[supports[i]]);
        }
    }

    return py::make_tuple(
            to_python(chain_w_expansions), to_python(indices), to_python(exps));
}

void p4t::pylog(string msg) { python_log()->info(msg); }

auto p4t::split(py::object classifier, int capacity, bool use_resolution)
        -> py::object {
    auto const rules = svmr2rules(classifier);

    auto [success, here, there] =
            opt::perform_best_splitting(rules, capacity, use_resolution);

    if (!success) {
        return py::make_tuple(py::object(), rules2svmr(rules));
    }

    return py::make_tuple(rules2svmr(here), rules2svmr(there));
}

auto p4t::try_boolean_minimization(py::object classifier, bool use_resolution)
        -> py::object {
    auto const rules = svmr2rules(classifier);
    auto const result = opt::boolean_minimization::perform_boolean_minimization(
            rules, true, use_resolution);
    return rules2svmr(result);
}

auto p4t::calc_obstruction_weights(py::object classifier) -> py::object {
    auto const rules = utils::svmr2rules(classifier);
    auto const weights =
            opt::boolean_minimization::calc_obstruction_weights(rules);
    py::dict result{};
    for (auto p : weights) {
        result[p.first.code()] = p.second;
    }
    return std::move(result);
}

auto p4t::incremental_updates(
        py::object new_classifier, py::list lpm_groups, py::object traditional,
        int tcam_size) -> py::tuple {
    auto const new_classifier_int = utils::svmr2filters(new_classifier);
    vector<pair<vector<Filter>, Support>> lpm_groups_int{};
    for (auto i = 0u; i < len(lpm_groups); ++i) {
        auto bits = lpm_groups[i].attr("bits");
        lpm_groups_int.emplace_back(
                utils::svmr2filters(lpm_groups[i]),
                Support(py::stl_input_iterator<int>(bits),
                        py::stl_input_iterator<int>()));
    }
    auto const traditional_int = utils::svmr2filters(traditional);
    auto const [num_added_groups, num_added_traditional] =
            opt::incremental_oi_lpm(
                    new_classifier_int, lpm_groups_int, traditional_int,
                    tcam_size);
    return py::make_tuple(num_added_groups, num_added_traditional);
}
