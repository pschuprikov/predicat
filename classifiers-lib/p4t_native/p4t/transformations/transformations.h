#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H

#include <concepts>

#include <p4t/model/filter.h>
#include <p4t/model/rule.h>
#include <p4t/model/support.h>

#include <p4t/common.h>
#include <type_traits>

namespace p4t::transformations {

class SubsetWithCommonMask {
  public:
    auto const &indices() const { return indices_; }
    auto const &mask() const { return mask_; }
    auto size() const { return indices().size(); }

    static auto
    from_old(vector<int> const &indices, model::Support const &support)
            -> SubsetWithCommonMask;

  private:
    SubsetWithCommonMask(model::Filter::BitArray mask, vector<int> indices);

  private:
    vector<int> indices_;
    model::Filter::BitArray mask_;
};

template <class T>
concept Maskable = requires(T const &x, model::Filter::BitArray const &mask) {
    { x.mask() }
    ->std::same_as<model::Filter::BitArray>;
    { x &mask }
    ->std::same_as<T>;
};

class SubsetWithMask {
  public:
    template <Maskable T>
    class Transformed;

    SubsetWithMask();
    SubsetWithMask(SubsetWithCommonMask const &common_mask);

    void push_back(model::Filter::BitArray x, int index);

    auto const &indices() const { return indices_; }
    auto const &masks() const { return masks_; }

    template <Maskable T>
    auto apply(vector<T> const &xs) const -> vector<T>;

  private:
    SubsetWithMask(vector<model::Filter::BitArray> masks, vector<int> indices);

  private:
    vector<int> indices_;
    vector<model::Filter::BitArray> masks_;
};

template <Maskable T>
class SubsetWithMask::Transformed {
  public:
    struct Entry {
        T element;
        int index;
    };

    struct Filters {
        struct Iterator {
            using difference_type = int;
            using value_type = T const;
            using iterator_category = std::random_access_iterator_tag;

            auto &operator*() const { return it_->element; }
            auto operator->() const { return &it_->element; }
            auto operator++(int) { return Iterator{it_++}; }
            auto operator--(int) { return Iterator{it_--}; }
            auto &operator++() { return ++it_, *this; }
            auto &operator--() { return --it_, *this; }
            auto operator-(int k) const { return Iterator{it_ - k}; }
            auto operator+(int k) const { return Iterator{it_ + k}; }
            friend auto operator+(int k, Iterator const &i) { return i + k; }
            auto &operator-=(int k) { return it_ -= k, *this; }
            auto &operator+=(int k) { return it_ += k, *this; }
            auto &operator[](int k) const { return it_[k].element; }

            auto operator-(Iterator const &other) const -> difference_type {
                return it_ - other.it_;
            }

            auto operator<=>(Iterator const &other) const = default;

            typename vector<Entry>::const_iterator it_;
        };

        static_assert(std::random_access_iterator<Iterator>);

        auto begin() const -> Iterator { return Iterator{begin_}; }

        auto end() const -> Iterator { return Iterator{end_}; }

        typename vector<Entry>::const_iterator begin_;
        typename vector<Entry>::const_iterator end_;
    };

    static_assert(std::ranges::input_range<Filters>);

  public:
    explicit Transformed(vector<T> const &elements = {});
    Transformed(Transformed const &) = default;

    template <class F>
    void apply(F f);

    template <class F>
    void remove_if(F f);

    template <class F>
    void sort(F f) requires std::relation<F, T const &, T const &>;

    template <class F>
    void remove_forward_if(F f) requires std::relation<F, T const &, T const &>;

    void reverse();

    template <class F>
    auto extract_if(F f) -> Transformed;
    auto extract(vector<int> const &indices) -> Transformed;

    auto pop_back() -> Entry;
    void push_back(Entry other);

    void add_all(Transformed const &other);

    void chain(SubsetWithMask const &transformation);

    auto back() const -> T;
    auto empty() const -> bool;
    auto filters() const -> Filters;
    auto size() const -> size_t;

    auto transformation() const -> SubsetWithMask;

  private:
    explicit Transformed(vector<Entry> indexed_filters);

  private:
    vector<Entry> indexed_filters_;
};

} // namespace p4t::transformations

#endif
