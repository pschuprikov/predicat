add_library(p4t_opt SHARED
    common.cpp

    model/support.cpp
    model/chain.cpp

    opt/chain_algos.cpp
    opt/oi_algos.cpp
    opt/boolean_minimization.cpp
    opt/expansion_algos.cpp
    opt/distribution_algos.cpp
    opt/intersections_opt.cpp
    opt/oi_lpm_algos.cpp
    opt/oi_lpm_algos/transformation_utils.cpp
    opt/oi_lpm_algos/greedy_all.cpp
    opt/oi_lpm_algos/original_supports.cpp
    opt/oi_lpm_algos/common.cpp
    opt/oi_lpm_algos/incremental_oi_lpm.cpp
    opt/updates.cpp

    transformations/transformations.cpp

)

target_link_libraries(p4t_opt PUBLIC Threads::Threads tbb spdlog::spdlog)
target_compile_features(p4t_opt PUBLIC cxx_std_20)

Python_add_library(p4t_native MODULE 
    p4t_native_ext.cpp

    utils/python_utils.cpp
    p4t_native.cpp
    )

target_link_libraries(p4t_native PUBLIC p4t_opt Boost::python38 Boost::numpy38)

