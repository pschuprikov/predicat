#ifndef COMMON_H
#define COMMON_H

#include <array>
#include <cassert>
#include <iostream>
#include <memory>
#include <optional>
#include <ranges>
#include <set>
#include <string>
#include <vector>

#include <spdlog/fmt/ostr.h>
#include <spdlog/spdlog.h>

namespace p4t {

auto constexpr MAX_WIDTH = 32 + 32 + 16 + 16 + 8;

using std::optional;
using std::pair;
using std::set;
using std::string;
using std::tuple;
using std::vector;
namespace ranges = std::ranges;

using std::make_pair;
using std::make_tuple;

auto sink() -> std::shared_ptr<spdlog::sinks::sink> const &;
auto log() -> std::shared_ptr<spdlog::logger> const &;
auto python_log() -> std::shared_ptr<spdlog::logger> const &;

template <ranges::input_range R>
auto to_vector(R r) {
    return vector(ranges::begin(r), ranges::end(r));
}

} // namespace p4t

#endif // COMMON_H
