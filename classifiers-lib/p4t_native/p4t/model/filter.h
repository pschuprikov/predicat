#ifndef FILTER_H
#define FILTER_H

#include <p4t/common.h>
#include <p4t/utils/bit_array.h>

#include <algorithm>
#include <ios>
#include <iterator>

namespace p4t::model {

enum class Bit { ONE, ZERO, ANY };

inline auto operator<<(std::ostream &out, Bit b) -> std::ostream &;

class Filter {
  public:
    using BitArray = utils::PackedBitArray<uint64_t, MAX_WIDTH>;

  public:
    explicit Filter(size_t width = 0) : value_{}, mask_{}, width_{width} {
        assert(width_ <= MAX_WIDTH);
    }

    Filter(BitArray const &value, BitArray const &mask, size_t width)
        : value_{value}, mask_{mask}, width_{width} {
        assert(width_ <= MAX_WIDTH);
    }

    auto size() const { return width_; }

    inline auto operator[](size_t i) const -> Bit;

    inline void set(size_t i, Bit value);

    inline auto has_any(BitArray const &mask) const -> bool;

    auto value() const { return value_; }

    auto mask() const { return mask_; }

  public:
    static inline auto
    fast_blocker(Filter const &f1, Filter const &f2, BitArray const &mask)
            -> pair<bool, int>;

    static auto fast_blocker(Filter const &f1, Filter const &f2)
            -> pair<bool, int>;

    static auto
    intersect(Filter const &lhs, Filter const &rhs, BitArray const &mask)
            -> bool;

    static auto intersect(Filter const &lhs, Filter const &rhs) -> bool;
    static auto subsums(Filter const &lhs, Filter const &rhs) -> bool;
    static auto
    subsums(Filter const &lhs, Filter const &rhs, BitArray const &mask) -> bool;

  private:
    BitArray value_;
    BitArray mask_;
    size_t width_;
};

template <class T> concept HasMask = requires(T const &x) {
    { x.mask() }
    ->std::same_as<Filter::BitArray>;
};

template <class T> concept HasValue = requires(T const &x) {
    { x.value() }
    ->std::same_as<Filter::BitArray>;
};

auto inline operator&(Filter const &lhs, Filter::BitArray const &mask)
        -> Filter;
auto inline operator==(Filter const &lhs, Filter const &rhs) -> bool;
auto inline operator<<(std::ostream &out, Filter const &filter)
        -> std::ostream &;

} // namespace p4t::model

namespace std {

template <> struct hash<p4t::model::Filter> {
    size_t operator()(p4t::model::Filter const &f) const {
        using namespace p4t::utils::hash;
        size_t result = std::hash<p4t::model::Filter::BitArray>()(f.mask());
        hash_combine(
                result, std::hash<p4t::model::Filter::BitArray>()(f.value()));
        return result;
    }
};

} // namespace std
#endif
