#ifndef FILTER_HPP
#define FILTER_HPP

#include "filter.h"

#include <p4t/utils/bit_array.hpp>

namespace p4t::model {

auto Filter::operator[](size_t i) const -> Bit {
    return mask_.get(i) ? (value_.get(i) ? Bit::ONE : Bit::ZERO) : Bit::ANY;
}

void Filter::set(size_t i, Bit value) {
    switch (value) {
    case Bit::ANY: {
        mask_.set(i, false);
    } break;
    case Bit::ONE: {
        mask_.set(i, true);
        value_.set(i, true);
    } break;
    case Bit::ZERO: {
        mask_.set(i, true);
        value_.set(i, false);
    } break;
    }
}

auto Filter::has_any(BitArray const &mask) const -> bool {
    using utils::num::testz;
    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        if (!testz(mask.chunk(i) & ~mask_.chunk(i))) {
            return true;
        }
    }
    return false;
}

auto Filter::fast_blocker(
        Filter const &f1, Filter const &f2, BitArray const &mask)
        -> pair<bool, int> {
    namespace num = utils::num;

    auto first_difference = -1;
    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        auto const diff =
                (f1.value_.chunk(i) ^ f2.value_.chunk(i)) &
                (mask.chunk(i) & f1.mask_.chunk(i) & f2.mask_.chunk(i));
        if (!num::testz(diff)) {
            if (first_difference != -1) {
                return {false, first_difference};
            }
            auto const idx = num::ctz(diff);
            first_difference = i * BitArray::BITS_PER_CHUNK + idx;
            if (!num::testz(
                        diff & ~num::kth_bit<BitArray::BitChunk>::value(idx))) {
                return {false, first_difference};
            }
        }
    }
    return {true, first_difference};
}

auto inline Filter::fast_blocker(Filter const &f1, Filter const &f2)
        -> pair<bool, int> {
    namespace num = utils::num;

    auto first_difference = -1;
    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        auto const diff = (f1.value_.chunk(i) ^ f2.value_.chunk(i)) &
                          (f1.mask_.chunk(i) & f2.mask_.chunk(i));
        if (!num::testz(diff)) {
            if (first_difference != -1) {
                return {false, first_difference};
            }
            auto const idx = num::ctz(diff);
            first_difference = i * BitArray::BITS_PER_CHUNK + idx;
            if (!num::testz(
                        diff & ~num::kth_bit<BitArray::BitChunk>::value(idx))) {
                return {false, first_difference};
            }
        }
    }
    return {true, first_difference};
}

auto inline Filter::intersect(
        Filter const &lhs, Filter const &rhs, BitArray const &mask) -> bool {
    namespace num = utils::num;

    assert(lhs.size() == rhs.size());

    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        if (!num::testz(
                    (lhs.value_.chunk(i) ^ rhs.value_.chunk(i)) &
                    mask.chunk(i) & lhs.mask_.chunk(i) & rhs.mask_.chunk(i))) {
            return false;
        }
    }

    return true;
}

auto inline Filter::intersect(Filter const &lhs, Filter const &rhs) -> bool {
    namespace num = utils::num;

    assert(lhs.size() == rhs.size());

    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        if (!num::testz(
                    (lhs.value_.chunk(i) ^ rhs.value_.chunk(i)) &
                    lhs.mask_.chunk(i) & rhs.mask_.chunk(i))) {
            return false;
        }
    }

    return true;
}

auto inline operator&(Filter const &lhs, Filter::BitArray const &mask)
        -> Filter {
    return Filter(lhs.value() & mask, lhs.mask() & mask, lhs.size());
}

auto inline Filter::subsums(Filter const &lhs, Filter const &rhs) -> bool {
    namespace num = utils::num;

    assert(lhs.size() == rhs.size());

    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        if (!num::testz(
                    ((lhs.value_.chunk(i) ^ rhs.value_.chunk(i)) |
                     ~rhs.mask_.chunk(i)) &
                    lhs.mask_.chunk(i))) {
            return false;
        }
    }

    return true;
}

auto inline Filter::subsums(
        Filter const &lhs, Filter const &rhs, BitArray const &mask) -> bool {
    namespace num = utils::num;

    assert(lhs.size() == rhs.size());

    for (auto i = 0u; i < BitArray::NUM_CHUNKS; i++) {
        if (!num::testz(
                    ((lhs.value_.chunk(i) ^ rhs.value_.chunk(i)) |
                     ~rhs.mask_.chunk(i) | ~mask.chunk(i)) &
                    lhs.mask_.chunk(i) & mask.chunk(i))) {
            return false;
        }
    }

    return true;
}

auto inline operator==(Filter const &lhs, Filter const &rhs) -> bool {
    return lhs.mask() == rhs.mask() &&
           (lhs.value() & lhs.mask()) == (rhs.value() & lhs.mask());
}

} // namespace p4t::model

inline auto p4t::model::operator<<(std::ostream &out, Bit b) -> std::ostream & {
    switch (b) {
    case Bit::ONE:
        return out << "1";
    case Bit::ZERO:
        return out << "0";
    case Bit::ANY:
        return out << "*";
    default:
        abort();
    }
}

auto inline p4t::model::operator<<(std::ostream &out, Filter const &filter)
        -> std::ostream & {
    for (auto i = 0u; i < filter.size(); i++) {
        out << filter[i];
    }
    return out;
}

#endif // FILTER_HPP
