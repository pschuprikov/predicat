#ifndef RULE_H
#define RULE_H

#include <p4t/model/filter.h>

#include <ostream>

namespace p4t::model {

class Action {
  public:
    Action(int code = -1) : code_{code} {}

    auto code() const { return code_; }

  public:
    static Action nop() { return Action{}; }

  private:
    int code_;
};

inline auto operator<<(std::ostream &out, Action const &act) -> std::ostream &;

inline auto operator==(Action const &lhs, Action const &rhs) {
    return rhs.code() == lhs.code();
}

inline auto operator!=(Action const &lhs, Action const &rhs) {
    return !(rhs == lhs);
}

inline auto operator<(Action const &lhs, Action const &rhs) {
    return lhs.code() < rhs.code();
}

class Rule {
  public:
    Rule(Filter filter, Action action) : filter_{filter}, action_{action} {}

    Rule() = default;

    auto &action() { return action_; }

    auto &filter() { return filter_; }

    auto mask() const { return filter().mask(); }

    auto value() const { return filter().value(); }

    auto action() const { return action_; }

    auto filter() const -> Filter { return filter_; }

  public:
    static inline auto
    intersect(Rule const &lhs, Rule const &rhs, Filter::BitArray const &mask)
            -> bool;
    static inline auto intersect(Rule const &lhs, Rule const &rhs) -> bool;
    static inline auto
    fast_blocker(Rule const &lhs, Rule const &rhs, Filter::BitArray const &mask)
            -> pair<bool, int>;

  private:
    Filter filter_;
    Action action_;
};

inline auto operator&(Rule const &rule, Filter::BitArray const &mask) -> Rule;
inline auto operator<<(std::ostream &out, Rule const &rule) -> std::ostream &;

} // namespace p4t::model

namespace std {

template <> struct hash<p4t::model::Action> {
    size_t operator()(p4t::model::Action const &action) const {
        return static_cast<size_t>(action.code());
    }
};

} // namespace std

#endif // RULE_H
