#include "support.h"

#include <p4t/model/support.hpp>

auto p4t::model::select_unique(vector<Support> supports) -> vector<Support> {
    std::sort(begin(supports), end(supports));
    auto last = std::unique(begin(supports), end(supports));
    supports.erase(last, end(supports));

    return supports;
}

auto p4t::model::weight(
        vector<Support> const &unique_supports,
        vector<Support> const &all_supports) -> vector<int> {
    support_map<int> support_cnt{};

    for (auto const &support : all_supports) {
        support_cnt[support]++;
    }

    vector<int> result(unique_supports.size());
    for (auto i = 0u; i < unique_supports.size(); i++) {
        result[i] = support_cnt[unique_supports[i]];
    }

    return result;
}

auto p4t::model::select_unique_n_weight(vector<Support> const &supports)
        -> pair<vector<Support>, vector<int>> {
    auto const unique = select_unique(supports);
    auto const weights = weight(unique, supports);
    return make_pair(unique, weights);
}

template <p4t::model::HasMask T>
auto p4t::model::to_supports(vector<T> const &filters) -> vector<Support> {
    vector<Support> supports{};
    std::transform(
            begin(filters), end(filters), back_inserter(supports),
            [](auto const &x) { return to_support(x); });
    return supports;
}

template auto p4t::model::to_supports(vector<p4t::model::Filter> const &)
        -> vector<Support>;
template auto p4t::model::to_supports(vector<p4t::model::Rule> const &)
        -> vector<Support>;
