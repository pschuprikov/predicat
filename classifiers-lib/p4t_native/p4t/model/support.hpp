#include "support.h"

#include <p4t/model/filter.hpp>
#include <p4t/model/rule.hpp>

#include <numeric>

template <p4t::model::HasMask T>
auto p4t::model::to_support(T const &x) -> Support {
    return to_support(x.mask());
}

extern template auto p4t::model::to_supports(vector<p4t::model::Rule> const &)
        -> vector<Support>;

extern template auto p4t::model::to_supports(vector<p4t::model::Filter> const &)
        -> vector<Support>;

auto p4t::model::to_support(std::tuple<int, int> const &bit_range) -> Support {
    auto const [start_bit, end_bit] = bit_range;
    Support result(end_bit - start_bit);
    std::iota(begin(result), end(result), start_bit);
    return result;
}

auto p4t::model::to_support(Filter::BitArray const &mask) -> Support {
    Support result{};
    auto const max_bits =
            Filter::BitArray::NUM_CHUNKS * Filter::BitArray::BITS_PER_CHUNK;
    for (auto i = 0; i < int(max_bits); i++) {
        if (mask.get(i)) {
            result.emplace_back(i);
        }
    }
    return result;
}

auto p4t::model::to_mask(Support const &support) -> Filter::BitArray {
    Filter::BitArray result{};
    for (auto i : support) {
        result.set(i, true);
    }
    return result;
}

inline auto p4t::model::operator<<(std::ostream &os, Support const &s)
        -> std::ostream & {
    return os << fmt::format("{}", s);
}

constexpr auto
fmt::formatter<p4t::model::Support>::parse(format_parse_context &ctx) {
    auto it = ctx.begin();
    if (it != ctx.end() && *it != '}')
        throw format_error("invalid format");
    return ctx.begin();
}

template <typename FormatContext>
auto fmt::formatter<p4t::model::Support>::format(
        const p4t::model::Support &support, FormatContext &ctx) {
    auto it = ctx.out();
    it = format_to(it, "{{");
    for (auto bit_it = begin(support); bit_it != end(support); ++bit_it) {
        it = format_to(it, "{}", *bit_it);
        if (bit_it + 1 != end(support)) {
            it = format_to(it, ",");
        }
    }
    it = format_to(it, "}}");
    return it;
}
