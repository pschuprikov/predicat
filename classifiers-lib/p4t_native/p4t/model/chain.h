#ifndef CHAIN_H
#define CHAIN_H

#include <p4t/common.h>
#include <p4t/model/support.h>

namespace p4t::model {

enum class ChainKind { ASCENDING, DESCENDING };

inline constexpr auto reverse(ChainKind kind) -> ChainKind {
    switch (kind) {
    case ChainKind::ASCENDING:
        return ChainKind::DESCENDING;
    case ChainKind::DESCENDING:
        return ChainKind::ASCENDING;
    }
}

template <ChainKind Kind> class Chain {
  public:
    Chain() = default;
    Chain(std::initializer_list<Support>);

    void push_back(Support const &support);

    auto empty() const { return chain_.empty(); };
    auto size() const { return chain_.size(); }
    auto reversed() const -> Chain<reverse(Kind)>;

    auto const &get_chain() const { return chain_; }

  private:
    vector<Support> chain_;
};

extern template class Chain<ChainKind::ASCENDING>;
extern template class Chain<ChainKind::DESCENDING>;

} // namespace p4t::model

#endif
