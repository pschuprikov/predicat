#ifndef BIT_ARRAY_HPP
#define BIT_ARRAY_HPP

#include "bit_array.h"

namespace p4t::utils {

template <class BitChunkT, size_t WidthT>
void PackedBitArray<BitChunkT, WidthT>::set(size_t i, bool x) {
    if (x) {
        bits_[i / BITS_PER_CHUNK] |=
                num::kth_bit<BitChunkT>::value(i % BITS_PER_CHUNK);
    } else {
        bits_[i / BITS_PER_CHUNK] &=
                ~(num::kth_bit<BitChunkT>::value(i % BITS_PER_CHUNK));
    }
}

template <class BitChunkT, size_t WidthT>
auto PackedBitArray<BitChunkT, WidthT>::get(size_t i) const -> bool {
    return !num::testz(
            bits_[i / BITS_PER_CHUNK] &
            (num::kth_bit<BitChunkT>::value(i % BITS_PER_CHUNK)));
}

template <class BitChunkT, size_t WidthT>
auto PackedBitArray<BitChunkT, WidthT>::chunk(size_t i) const -> BitChunkT {
    return bits_[i];
}

template <class BitChunkT, size_t WidthT>
auto PackedBitArray<BitChunkT, WidthT>::chunk(size_t i) -> BitChunkT & {
    return bits_[i];
}

template <class BitChunkT, size_t WidthT>
auto PackedBitArray<BitChunkT, WidthT>::ones()
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = ~0;
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
auto PackedBitArray<BitChunkT, WidthT>::zeros()
        -> PackedBitArray<BitChunkT, WidthT> {
    return PackedBitArray<BitChunkT, WidthT>{};
}

template <class BitChunkT, size_t WidthT>
inline auto operator&(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = lhs.chunk(i) & rhs.chunk(i);
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto operator-(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = lhs.chunk(i) & (~rhs.chunk(i));
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto operator~(PackedBitArray<BitChunkT, WidthT> const &arr)
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = ~arr.chunk(i);
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto operator^(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = lhs.chunk(i) ^ rhs.chunk(i);
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto operator|(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT> {
    PackedBitArray<BitChunkT, WidthT> result{};
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result.chunk(i) = lhs.chunk(i) | rhs.chunk(i);
    }

    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto is_zero(PackedBitArray<BitChunkT, WidthT> const &arr) -> bool {
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        if (!num::testz(arr.chunk(i))) {
            return false;
        }
    }
    return true;
}

template <class BitChunkT, size_t WidthT>
inline auto operator==(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool {
    return is_zero(lhs ^ rhs);
}

template <class BitChunkT, size_t WidthT>
inline auto operator!=(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool {
    return !(lhs == rhs);
}

template <class BitChunkT, size_t WidthT>
inline auto operator<(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool {
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        if (num::less(lhs.chunk(i), rhs.chunk(i))) {
            return true;
        }
        if (!num::testz(lhs.chunk(i) ^ rhs.chunk(i))) {
            return false;
        }
    }
    return false;
}

template <class BitChunkT, size_t WidthT>
inline auto popcount(PackedBitArray<BitChunkT, WidthT> const &arr) -> size_t {
    size_t result = 0;
    for (auto i = 0u; i < PackedBitArray<BitChunkT, WidthT>::NUM_CHUNKS; i++) {
        result += num::popcount(arr.chunk(i));
    }
    return result;
}

template <class BitChunkT, size_t WidthT>
inline auto
operator<<(std::ostream &os, PackedBitArray<BitChunkT, WidthT> const &arr) {
    for (auto i = 0u; i < WidthT; i++) {
        os << arr.get(i);
    }
}

} // namespace p4t::utils

template <typename BitChunkT, size_t WidthT>
constexpr auto
fmt::formatter<p4t::utils::PackedBitArray<BitChunkT, WidthT>>::parse(
        format_parse_context &ctx) {
    auto it = ctx.begin();
    if (it != ctx.end() && *it != '}')
        throw format_error("invalid format");
    return ctx.begin();
}

template <typename BitChunkT, size_t WidthT>
template <typename FormatContext>
auto fmt::formatter<p4t::utils::PackedBitArray<BitChunkT, WidthT>>::format(
        const p4t::utils::PackedBitArray<BitChunkT, WidthT> &arr,
        FormatContext &ctx) {
    auto it = ctx.out();
    for (auto i = 0; i < int(WidthT); i++) {
        it = format_to(it, "{}", arr.get(i) ? '1' : '0');
    }
    return it;
}

#endif // BIT_ARRAY_HPP
