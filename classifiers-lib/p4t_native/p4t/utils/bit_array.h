#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

#include <p4t/utils/bits_aux.h>
#include <p4t/utils/hashing_aux.h>

#include <array>

namespace p4t::utils {

template <class BitChunkT, size_t WidthT> struct PackedBitArray {
    using BitChunk = BitChunkT;

    static auto constexpr WIDTH = WidthT;
    static auto constexpr BITS_PER_CHUNK = 8 * sizeof(BitChunk);
    static auto constexpr NUM_CHUNKS =
            (WIDTH + BITS_PER_CHUNK - 1) / BITS_PER_CHUNK;

    static auto ones() -> PackedBitArray;
    static auto zeros() -> PackedBitArray;

  public:
    PackedBitArray() = default;

    void set(size_t i, bool x);
    auto get(size_t i) const -> bool;
    auto chunk(size_t i) const -> BitChunk;
    auto chunk(size_t i) -> BitChunk &;

    friend struct std::hash<PackedBitArray>;

  private:
    std::array<BitChunk, NUM_CHUNKS> bits_;
};

template <class BitChunkT, size_t WidthT>
inline auto operator&(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT>;

template <class BitChunkT, size_t WidthT>
inline auto operator-(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT>;

template <class BitChunkT, size_t WidthT>
inline auto operator~(PackedBitArray<BitChunkT, WidthT> const &arr)
        -> PackedBitArray<BitChunkT, WidthT>;

template <class BitChunkT, size_t WidthT>
inline auto operator^(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT>;

template <class BitChunkT, size_t WidthT>
inline auto operator|(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs)
        -> PackedBitArray<BitChunkT, WidthT>;

template <class BitChunkT, size_t WidthT>
inline auto is_zero(PackedBitArray<BitChunkT, WidthT> const &arr) -> bool;

template <class BitChunkT, size_t WidthT>
inline auto operator==(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool;

template <class BitChunkT, size_t WidthT>
inline auto operator!=(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool;

template <class BitChunkT, size_t WidthT>
inline auto operator<(
        PackedBitArray<BitChunkT, WidthT> const &lhs,
        PackedBitArray<BitChunkT, WidthT> const &rhs) -> bool;

template <class BitChunkT, size_t WidthT>
inline auto popcount(PackedBitArray<BitChunkT, WidthT> const &arr) -> size_t;

template <class BitChunkT, size_t WidthT>
inline auto
operator<<(std::ostream &os, PackedBitArray<BitChunkT, WidthT> const &arr);

} // namespace p4t::utils

template <class BitChunkT, size_t WidthT>
struct fmt::formatter<p4t::utils::PackedBitArray<BitChunkT, WidthT>> {
    constexpr auto parse(format_parse_context &ctx);

    template <typename FormatContext>
    auto
    format(const p4t::utils::PackedBitArray<BitChunkT, WidthT> &arr,
           FormatContext &ctx);
};

namespace std {

template <class BitChunkT, size_t WidthT>
struct hash<p4t::utils::PackedBitArray<BitChunkT, WidthT>> {
    size_t
    operator()(p4t::utils::PackedBitArray<BitChunkT, WidthT> const &pba) const {
        return p4t::utils::hash::hash_array(pba.bits_);
    }
};

} // namespace std

#endif // BIT_ARRAY_H
