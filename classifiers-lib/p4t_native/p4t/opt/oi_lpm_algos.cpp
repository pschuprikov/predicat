#include "oi_lpm_algos.h"

#include <p4t/model/chain.h>
#include <p4t/model/filter.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/chain_algos.h>
#include <p4t/opt/chain_basic_algos.hpp>
#include <p4t/opt/oi_algos.h>
#include <p4t/opt/oi_lpm_algos/greedy_all.h>
#include <p4t/opt/oi_lpm_algos/incremental_oi_lpm.h>
#include <p4t/opt/oi_lpm_algos/original_supports.h>
#include <p4t/opt/oi_lpm_algos/transformation_utils.h>
#include <p4t/transformations/transformations.hpp>
#include <p4t/utils/bit_array.hpp>
#include <p4t/utils/timer.h>

#include <algorithm>
#include <execution>
#include <numeric>

using namespace p4t;
using namespace opt;
using namespace oi_lpm_algos;

namespace {

template <class ForwardIt>
auto get_max_mask_size(ForwardIt first, ForwardIt beyond) -> int {
    return std::transform_reduce(
            std::execution::seq, first, beyond, 0,
            [](auto a, auto b) { return std::max(a, b); },
            [](auto const &f) { return int(popcount(f.mask())); });
}

template <class ForwardIt>
auto get_used_bits(ForwardIt first, ForwardIt beyond)
        -> model::Filter::BitArray {
    return std::transform_reduce(
            std::execution::seq, first, beyond, model::Filter::BitArray{},
            [](auto a, auto b) { return a | b; },
            [](auto const &f) { return f.mask(); });
}

template <class T>
auto find_oi_lpm_subset_aux_dispatch(
        vector<T> const &filters, OiLpmParams params)
        -> std::pair<
                transformations::SubsetWithMask,
                model::Chain<model::ChainKind::DESCENDING>> {
    switch (params.method) {
    case OiLpmMethod::GREEDY_ALL:
        return find_oi_lpm_subset_greedy_all(
                filters, params.max_support_size,
                params.greedy_strategy.value());
    case OiLpmMethod::ORIGINAL_SUPPORTS:
        return find_oi_lpm_subset_original_supports(
                filters, params.max_support_size);
    case OiLpmMethod::INCREMENTAL:
        return find_oi_lpm_incremental(filters);
    }
}

template <model::ChainKind Kind>
auto intersect(model::Chain<Kind> const &chain, model::Support base)
        -> model::Chain<Kind> {
    auto result = model::Chain<Kind>{};
    for (auto support : chain.get_chain()) {
        result.push_back(model::get_intersection(support, base));
    }
    return result;
}

} // namespace

template <class T>
auto p4t::opt::find_oi_lpm_subset(vector<T> const &filters, OiLpmParams params)
        -> OiLpmResult {
    auto filters_transformed =
            transformations::SubsetWithMask::Transformed(filters);

    if (params.max_support_size &&
        params.width_reduction_order == OiLpmWidthReductionOrder::BEFORE) {
        auto const oi_transform = reduce_width(
                to_vector(filters_transformed.filters()),
                params.max_support_size.value());
        filters_transformed.chain(oi_transform);
    }

    auto [oi_lpm_transform, chain] = find_oi_lpm_subset_aux_dispatch(
            to_vector(filters_transformed.filters()),
            {.max_support_size = std::nullopt,
             .method = params.method,
             .greedy_strategy = params.greedy_strategy});
    filters_transformed.chain(oi_lpm_transform);

    if (params.max_support_size &&
        params.width_reduction_order == OiLpmWidthReductionOrder::AFTER) {
        auto const oi_transform = reduce_width(
                to_vector(filters_transformed.filters()),
                params.max_support_size.value());
        filters_transformed.chain(oi_transform);
        chain = intersect(chain, model::to_support(oi_transform.mask()));
    }

    auto const &xs = to_vector(filters_transformed.filters());
    verify_oi(xs);
    verify_chain<model::ChainKind::DESCENDING>(xs);

    return make_pair(filters_transformed.transformation(), chain);
}

template auto p4t::opt::find_oi_lpm_subset(
        vector<model::Filter> const &xs, OiLpmParams params) -> OiLpmResult;
template auto
p4t::opt::find_oi_lpm_subset(vector<model::Rule> const &xs, OiLpmParams params)
        -> OiLpmResult;
