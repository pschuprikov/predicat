#include <algorithm>
#include <execution>
#include <functional>
#include <numeric>

#include <optional>
#include <p4t/model/filter.hpp>
#include <p4t/model/rule.hpp>
#include <p4t/model/support.hpp>
#include <p4t/utils/bit_array.hpp>
#include <p4t/utils/collections.h>

#include "oi_algos.h"

#include <spdlog/fmt/ostr.h>

namespace {

using namespace p4t;
using namespace opt;
using namespace model;

// TODO: merge with the one in transformations.h
//

auto filter(model::Filter const &filter) { return filter; }

auto filter(model::Rule const &rule) { return rule.filter(); }

template <class T>
concept Maskable = requires(T const &x, model::Filter::BitArray const &mask) {
    { x.mask() }
    ->std::same_as<model::Filter::BitArray>;
    { x &mask }
    ->std::same_as<T>;
};

template <class T> concept HasFilter = requires(T const &x) {
    { filter(x) }
    ->std::same_as<model::Filter>;
};

template <class T>
concept FastBlockable =
        requires(T const &x, model::Filter::BitArray const &mask) {
    { T::fast_blocker(x, x, mask) }
    ->std::same_as<std::pair<bool, int>>;
};

auto oi_log() -> std::shared_ptr<spdlog::logger> const & {
    static auto initialized = false;
    static std::shared_ptr<spdlog::logger> logger{};

    if (!initialized) {
        logger = std::make_shared<spdlog::logger>("oi", sink());
        logger->flush_on(spdlog::level::warn);
        logger->set_level(spdlog::level::warn);
        initialized = true;
    }
    return logger;
}

auto calc_set_difference(vector<int> const &lhs, vector<int> const &rhs) {
    vector<int> tmp;
    std::set_difference(
            begin(lhs), end(lhs), begin(rhs), end(rhs), back_inserter(tmp));
    return tmp;
}

template <class T>
void take_subset(
        vector<T> &xs, vector<int> &indices, vector<int> subset_indices) {
    xs = utils::subset(xs, subset_indices);
    indices = utils::subset(indices, subset_indices);
}

template <Maskable T>
auto get_bits_in_use(
        vector<T> const &filters, std::optional<model::Support> bit_range)
        -> vector<int> {
    if (bit_range) {
        return *bit_range;
    } else {
        auto largest_mask = std::transform_reduce(
                std::execution::seq, begin(filters), end(filters),
                Filter::BitArray(),
                [](Filter::BitArray a, Filter::BitArray b) { return a | b; },
                [](auto f) { return f.mask(); });
        return to_support(largest_mask);
    }
}

template <HasFilter T>
auto find_exact(vector<T> const &xs, vector<int> const &bits_in_use) {
    auto exact = bits_in_use;
    for (auto const &x : xs) {
        for (auto const bit : bits_in_use) {
            if (filter(x)[bit] == Bit::ANY) {
                exact.erase(remove(begin(exact), end(exact), bit), end(exact));
            }
        }
    }
    return exact;
}

template <HasFilter T>
auto calc_bit_stats(vector<T> const &xs, vector<int> const &bits_in_use) {
    BitStats stats{filter(xs[0]).size()};

    for (auto const &x : xs) {
        for (auto i : bits_in_use) {
            switch (filter(x)[i]) {
            case Bit::ANY:
                stats.dontcare[i]++;
                break;
            case Bit::ONE:
                stats.ones[i]++;
                break;
            case Bit::ZERO:
                stats.zeros[i]++;
            }
        }
    }
    for (auto i : bits_in_use) {
        if (stats.dontcare[i] == 0) {
            stats.exact_bits.emplace_back(i);
        }
    }

    return stats;
}

[[maybe_unused]] auto
is_oi(vector<Filter> const &filters, vector<int> const &bits_in_use) {
    vector<int> indices(filters.size());
    iota(begin(indices), end(indices), 0);
    vector<bool> has_intersection(filters.size());

    auto const mask = bits_to_mask(bits_in_use);
    for (auto i = begin(filters); i != end(filters); ++i) {
        for (auto j = begin(filters); j != i; j++) {
            if (Filter::intersect(*i, *j, mask)) {
                return false;
            }
        }
    }
    return true;
}

template <class T>
requires HasFilter<T> &&FastBlockable<T> auto
find_blockers(vector<T> const &filters, vector<int> const &bits_in_use) {
    oi_log()->info("Calculating blockers...");

    vector<vector<bool>> blockers(
            filters.size(), vector<bool>(filter(filters[0]).size(), false));

    vector<int> indices(filters.size());
    iota(begin(indices), end(indices), 0);

    auto const bits_mask = bits_to_mask(bits_in_use);

    std::for_each(
            std::execution::par, begin(indices), end(indices),
            [&filters, mask = bits_mask, &blockers](auto i) {
                auto const &lower = filters[i];
                for (auto j = 0; j < i; j++) {
                    auto const res = T::fast_blocker(lower, filters[j], mask);

                    if (res.second == -1) {
                        blockers[i].assign(blockers[i].size(), true);
                        break;
                    } else if (res.first) {
                        blockers[i][res.second] = true;
                    }
                }
            });

    oi_log()->info("...Finished");

    return blockers;
}

template <class Function, class Cmp>
auto find_best_bits(
        vector<int> const &all_bits, vector<int> try_last, Function value,
        Cmp cmp_value, int k = 1) -> vector<int> {

    auto const cmp = [cmp_value = cmp_value, value = value](auto a, auto b) {
        return cmp_value(value(a), value(b));
    };

    vector<int> try_first = calc_set_difference(all_bits, try_last);

    vector<int> best{};

    if (int(try_first.size()) > k) {
        std::nth_element(
                begin(try_first), begin(try_first) + k, end(try_first), cmp);
        std::copy(begin(try_first), begin(try_first) + k, back_inserter(best));
    } else {
        std::copy(begin(try_first), end(try_first), back_inserter(best));
    }

    if (int(best.size()) <= k) {
        if (int(try_last.size() + best.size()) > k) {
            std::nth_element(
                    begin(try_last), begin(try_last) + k - best.size(),
                    end(try_last), cmp);
            std::copy(
                    begin(try_last), begin(try_last) + k - best.size(),
                    back_inserter(best));
        } else {
            std::copy(begin(try_last), end(try_last), back_inserter(best));
        }
    }

    std::sort(begin(best), end(best));

    return best;
}

template <class Function, class Cmp>
auto find_best_bit(
        vector<int> const &all_bits, vector<int> const &try_last,
        Function value, Cmp cmp_value) -> int {
    return find_best_bits(all_bits, try_last, value, cmp_value, 1).front();
}

[[maybe_unused]] auto check_if_use_dontcare_heuristic(
        vector<int> const &bits_in_use, vector<int> const &bit_num_blockers,
        size_t l) {
    log()->info("Checking whether to use don't care heuristic...");
    if (bits_in_use.size() > 2 * l) {
        auto indices_sorted_by_blockers = bits_in_use;

        std::sort(
                begin(indices_sorted_by_blockers),
                end(indices_sorted_by_blockers),
                [&bit_num_blockers](int a, int b) {
                    return bit_num_blockers[a] < bit_num_blockers[b];
                });

        if (bit_num_blockers[indices_sorted_by_blockers[0]] >=
            0.9 * bit_num_blockers[indices_sorted_by_blockers[2 * l]]) {
            log()->info("...YES, use heuristic!");
            return true;
        }
    }

    log()->info("...NO, don't use heuristic!");
    return false;
}

template <class Blockers> auto sum_blockers_by_bit(Blockers const &blockers) {
    vector<int> bit_num_blockers(blockers[0].size());

    for (auto const &blocker : blockers) {
        for (auto i = 0u; i < blocker.size(); i++) {
            if (blocker[i]) {
                bit_num_blockers[i]++;
            }
        }
    }

    return bit_num_blockers;
}

[[maybe_unused]] auto apply_dont_care_heuristic(
        vector<Filter> filters, vector<int> const &bits_in_use,
        BitStats const &stats, bool only_exact, size_t l, OIMode oi_mode)
        -> tuple<bool, vector<int>, vector<int>> {

    log()->info("Using ANY HEURISTIC!");

    auto const rm_bits = find_best_bits(
            bits_in_use, only_exact ? stats.exact_bits : vector<int>{},
            [&stats](auto b) {
                return std::min(stats.zeros[b], stats.ones[b]);
            },
            std::less<>(), bits_in_use.size() - l);

    auto cur_mask = bits_to_mask(bits_in_use);
    for (auto bit : rm_bits) {
        cur_mask.set(bit, false);
    }

    if (only_exact) {
        vector<int> const exact_indices = [&]() {
            vector<int> retval;
            for (auto i = 0; i < int(filters.size()); i++) {
                if (filters[i].has_any(cur_mask)) {
                    continue;
                }
                retval.emplace_back(i);
            }
            return retval;
        }();

        if (exact_indices.size() < 0.001 * filters.size()) {
            log()->info(
                    "...any heuristinc FAILED, found only {:d} indices",
                    exact_indices.size());
            return make_tuple(false, vector<int>{}, vector<int>{});
        }

        vector<int> indices(filters.size());
        iota(begin(indices), end(indices), 0);
        take_subset(filters, indices, exact_indices);

        take_subset(
                filters, indices,
                find_maximal_oi_subset(filters, cur_mask, oi_mode));

        log()->info(
                "...found exact OI indices with {:d}/{:d} bits, exact {:d}, OI "
                "{:d}",
                bits_in_use.size() - rm_bits.size(), bits_in_use.size(),
                exact_indices.size(), indices.size());
        return make_tuple(true, rm_bits, indices);
    }

    auto const oi_indices = find_maximal_oi_subset(filters, cur_mask, oi_mode);
    log()->info(
            "...checking OI indices with {:d}/{:d} bits, OI {:d}",
            bits_in_use.size() - rm_bits.size(), bits_in_use.size(),
            oi_indices.size());
    return make_tuple(true, rm_bits, oi_indices);
}

template <class T, class Cmp>
auto log_best_elements(
        std::string_view msg, int idx, vector<T> const &xs, Cmp cmp,
        vector<int> const &bits_in_use) {
    auto indices = bits_in_use;
    std::sort(begin(indices), end(indices), [cmp, &xs](auto a, auto b) {
        return cmp(xs[a], xs[b]);
    });
    oi_log()->info(
            "...{}: [{:d}, {:d}, {:d}, ..., {:d}]", msg, xs[indices[0]],
            xs[indices[1]], xs[indices[2]], xs[indices[idx]]);
}

template <class T>
auto remove_bits_w_blockers(
        vector<T> const filters, vector<int> const &bits_in_use,
        BitStats const &stats, bool only_exact, size_t l,
        std::optional<Support> bits_lower) -> pair<vector<int>, vector<int>> {
    auto const blockers = find_blockers(filters, bits_in_use);
    auto const bit_num_blockers = sum_blockers_by_bit(blockers);

    log_best_elements(
            "dontcares", l, stats.dontcare, std::greater<int>(), bits_in_use);
    log_best_elements(
            "blockers", l, bit_num_blockers, std::less<int>(), bits_in_use);

    // if (check_if_use_dontcare_heuristic(bits_in_use, bit_num_blockers, l)) {
    //    auto [success, bits_to_remove, oi_indices] =
    //        apply_dont_care_heuristic(filters, bits_in_use, stats, only_exact,
    //        l);
    //    if (success) {
    //        return make_pair(bits_to_remove, oi_indices);
    //    }
    //}
    auto const preferred_bits = get_union(
            only_exact ? stats.exact_bits : Support{},
            bits_lower ? *bits_lower : Support{});

    auto const best_bit = find_best_bit(
            bits_in_use, preferred_bits,
            [&bit_num_blockers, &stats](auto bit) {
                return make_pair(bit_num_blockers[bit], -stats.dontcare[bit]);
            },
            std::less<>());

    vector<int> result{};

    for (auto i = 0u; i < blockers.size(); i++) {
        if (!blockers[i][best_bit]) {
            result.emplace_back(i);
        }
    }

    oi_log()->info(
            "Best bit is {:d} with {:d} rules and {:d} ANY bits", best_bit,
            result.size(), stats.dontcare[best_bit]);

    return {{best_bit}, result};
}

template <class T>
auto remove_bits_oi(
        vector<T> const &filters, vector<int> const &bits_in_use,
        BitStats const &stats, bool only_exact, OIMode oi_mode,
        std::optional<Support> bits_lower) -> pair<vector<int>, vector<int>> {

    auto mask = bits_to_mask(bits_in_use);
    auto const preferred_bits = get_union(
            only_exact ? stats.exact_bits : Support{},
            bits_lower ? *bits_lower : Support{});

    auto best = find_best_bits(
            bits_in_use, preferred_bits,
            [&mask, &filters, oi_mode](auto bit) {
                auto cur_mask = mask;
                cur_mask.set(bit, false);
                return int(find_maximal_oi_subset(filters, cur_mask, oi_mode)
                                   .size());
            },
            std::greater<>());

    mask.set(best.front(), false);
    return make_pair(best, find_maximal_oi_subset(filters, mask, oi_mode));
}

template <Intersectable T>
auto find_maximal_oi_subset_degree(
        vector<T> const &xs, Filter::BitArray const &mask) -> vector<int> {
    vector<int> result{};

    vector<int> indices(xs.size());
    iota(begin(indices), end(indices), 0);

    vector<int> num_edges(xs.size());
    std::for_each(
            std::execution::par, begin(indices), end(indices),
            [&xs, &num_edges, mask](auto i) {
                for (int j = 0; j < int(xs.size()); j++) {
                    if (i != j && T::intersect(xs[j], xs[i], mask)) {
                        num_edges[i]++;
                    }
                }
            });

    while (!indices.empty()) {
        auto min_idx = *min_element(
                begin(indices), end(indices), [&num_edges](auto a, auto b) {
                    return num_edges[a] < num_edges[b];
                });
        result.emplace_back(min_idx);

        auto rm_it = std::stable_partition(
                begin(indices), end(indices), [&xs, min_idx, mask](auto i) {
                    return !T::intersect(xs[i], xs[min_idx], mask);
                });

        std::for_each(
                std::execution::par, begin(indices), rm_it,
                [&xs, &num_edges, mask, beg = rm_it,
                 end = end(indices)](auto i) {
                    for (auto it = beg; it != end; ++it) {
                        if (T::intersect(xs[i], xs[*it], mask)) {
                            num_edges[i]--;
                        }
                    }
                });

        indices.erase(rm_it, end(indices));
    }

    return result;
}

template <Intersectable T>
auto find_maximal_oi_subset_top_down(
        vector<T> const &xs, Filter::BitArray const &mask) -> vector<int> {
    vector<int> result{};

    for (auto i = 0u; i < xs.size(); i++) {
        auto intersects = false;
        for (auto j : result) {
            if (T::intersect(xs[j], xs[i], mask)) {
                intersects = true;
                break;
            }
        }
        if (!intersects) {
            result.emplace_back(i);
        }
    }

    return result;
}

template <class T>
auto best_to_stay_minme_aux_bit_range(vector<T> filters, OIParams params, BitRange bit_range)
        -> transformations::SubsetWithCommonMask {
    using model::operator<<;

    log()->info(
            "starting minme; mode: {:d}; only exact: {:b}; total filters: "
            "{:d}; bitsrange: [{},{}]",
            params.mode, params.only_exact, filters.size(),
            bit_range.lower.value_or(Support{}),
            bit_range.upper.value_or(Support{}));

    vector<int> indices(filters.size());
    iota(begin(indices), end(indices), 0);

    auto bits_in_use = get_bits_in_use(filters, bit_range.upper);

    auto exact_bits_in_use = find_exact(filters, bits_in_use);

    take_subset(
            filters, indices,
            find_maximal_oi_subset(
                    filters, bits_to_mask(bits_in_use), params.oi_mode));

    log()->info(
            "We were left with {:d} OI filters and {:d} bits", indices.size(),
            bits_in_use.size());

    while (bits_in_use.size() > params.l ||
           (params.only_exact && bits_in_use != exact_bits_in_use)) {
        auto const stats = ::calc_bit_stats(filters, bits_in_use);

        vector<int> rm_bits;
        vector<int> oi_indices;
        switch (params.mode) {
        case MinMEMode::MAX_OI:
            tie(rm_bits, oi_indices) = remove_bits_oi(
                    filters, bits_in_use, stats, params.only_exact,
                    params.oi_mode, bit_range.lower);
            break;
        case MinMEMode::BLOCKERS:
            tie(rm_bits, oi_indices) = remove_bits_w_blockers(
                    filters, bits_in_use, stats, params.only_exact, params.l,
                    bit_range.lower);
            break;
        }

        bits_in_use = calc_set_difference(bits_in_use, rm_bits);

        take_subset(filters, indices, oi_indices);

        exact_bits_in_use = find_exact(filters, bits_in_use);

        log()->info(
                "bits [{:d}...] have been found;"
                " bits left: {:d}; exact bits left: {:d}; entries left: {:d}",
                rm_bits.front(), bits_in_use.size(), exact_bits_in_use.size(),
                filters.size());
    }

    return transformations::SubsetWithCommonMask::from_old(
            indices, bits_in_use);
}

template <class T>
auto best_to_stay_minme_aux(vector<T> filters, OIParams params)
        -> transformations::SubsetWithCommonMask {
    if (params.bit_ranges.empty()) {
        throw std::logic_error("bit ranges must never be empty");
    }
    auto best_result = std::optional<transformations::SubsetWithCommonMask>{};
    for (auto bit_range : params.bit_ranges) {
        auto result = best_to_stay_minme_aux_bit_range(filters, params, bit_range);
        if (!best_result || result.size() > best_result->size()) {
            best_result = result;
        }
    }
    return *best_result;
}

} // namespace

auto p4t::opt::best_min_similarity_bits(vector<Filter> const &filters, size_t l)
        -> vector<int> {
    vector<int> result{};
    while (result.size() < l) {
        auto best_bit = -1;
        auto best_value = -1;
        for (auto i = 0u; i < filters[0].size(); i++) {
            if (find(begin(result), end(result), i) != end(result)) {
                continue;
            }
            auto count_zero = 0;
            auto count_one = 0;
            for (auto const &f : filters) {
                if (f[i] == Bit::ANY || f[i] == Bit::ONE) {
                    count_one++;
                }
                if (f[i] == Bit::ANY || f[i] == Bit::ZERO) {
                    count_zero++;
                }
            }
            auto const value = std::max(count_zero, count_one);
            if (best_bit == -1 || value < best_value) {
                best_bit = i;
                best_value = value;
            }
        }
        result.emplace_back(best_bit);
    }

    return result;
}

auto p4t::opt::best_to_stay_minme(vector<model::Rule> rules, OIParams params)
        -> transformations::SubsetWithCommonMask {
    return best_to_stay_minme_aux(rules, params);
}

auto p4t::opt::best_to_stay_minme(vector<Filter> filters, OIParams params)
        -> transformations::SubsetWithCommonMask {
    return best_to_stay_minme_aux(filters, params);
}

template <Intersectable T>
auto p4t::opt::find_maximal_oi_subset(
        vector<T> const &xs, Filter::BitArray const &mask, OIMode mode)
        -> vector<int> {
    log()->info("Looking for a maximal oi subset...");

    vector<int> result{};
    switch (mode) {
    case OIMode::TOP_DOWN:
        result = find_maximal_oi_subset_top_down(xs, mask);
        break;
    case OIMode::MIN_DEGREE:
        result = find_maximal_oi_subset_degree(xs, mask);
        break;
    default:
        abort();
    }

    log()->info("...Finished");
    return result;
}

auto p4t::opt::calc_bit_stats(vector<Filter> const &filters) -> BitStats {
    if (filters.empty()) {
        return BitStats(0);
    }
    vector<int> bits_in_use(filters[0].size());
    std::iota(begin(bits_in_use), end(bits_in_use), 0);
    return ::calc_bit_stats(filters, bits_in_use);
}

auto p4t::opt::bits_to_mask(vector<int> const &bits) -> Filter::BitArray {
    Filter::BitArray res{};
    for (auto bit : bits) {
        res.set(bit, true);
    }
    return res;
}

template <Intersectable T> auto p4t::opt::is_oi(vector<T> const &xs) -> bool {
    for (auto i = begin(xs); i != end(xs); ++i) {
        for (auto j = begin(xs); j != i; j++) {
            if (T::intersect(*i, *j)) {
                return false;
            }
        }
    }
    return true;
}

template <Intersectable T> void p4t::opt::verify_oi(vector<T> const &xs) {
    if (!is_oi(xs)) {
        log()->critical("result is not order independend!");
        for (auto const &x : xs) {
            log()->critical("{}", x);
        }
        throw std::logic_error("result is not order independend!");
    }
}

template auto p4t::opt::find_maximal_oi_subset(
        vector<model::Rule> const &, model::Filter::BitArray const &, OIMode)
        -> vector<int>;
template auto p4t::opt::find_maximal_oi_subset(
        vector<model::Filter> const &, model::Filter::BitArray const &, OIMode)
        -> vector<int>;
template auto p4t::opt::is_oi(vector<model::Filter> const &) -> bool;
template auto p4t::opt::is_oi(vector<model::Rule> const &) -> bool;
template void p4t::opt::verify_oi(vector<model::Filter> const &);
template void p4t::opt::verify_oi(vector<model::Rule> const &);
