#ifndef OI_LPM_ALGOS_H
#define OI_LPM_ALGOS_H

#include <p4t/common.h>
#include <p4t/model/chain.h>
#include <p4t/model/filter.h>
#include <p4t/opt/oi_lpm_algos/greedy_all.h>
#include <p4t/transformations/transformations.h>

namespace p4t::opt {

using oi_lpm_algos::GreedyStrategy;

enum class OiLpmMethod { GREEDY_ALL, ORIGINAL_SUPPORTS, INCREMENTAL };

enum class OiLpmWidthReductionOrder { BEFORE, AFTER };

struct OiLpmParams {
    std::optional<int> max_support_size = std::nullopt;
    OiLpmMethod method = OiLpmMethod::GREEDY_ALL;
    std::optional<GreedyStrategy> greedy_strategy = GreedyStrategy::MAX_LPM;
    OiLpmWidthReductionOrder width_reduction_order =
            OiLpmWidthReductionOrder::BEFORE;
};

template <class T>
auto find_oi_lpm_subset(vector<T> const &xs, OiLpmParams params)
        -> oi_lpm_algos::OiLpmResult;

extern template auto
find_oi_lpm_subset(vector<model::Filter> const &xs, OiLpmParams params)
        -> oi_lpm_algos::OiLpmResult;
extern template auto
find_oi_lpm_subset(vector<model::Rule> const &xs, OiLpmParams params)
        -> oi_lpm_algos::OiLpmResult;
} // namespace p4t::opt

#endif
