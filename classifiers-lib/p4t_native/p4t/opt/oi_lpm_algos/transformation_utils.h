#ifndef OI_LPM_ALGOS_TRANSFORMATION_UTILS_H
#define OI_LPM_ALGOS_TRANSFORMATION_UTILS_H

#include <p4t/common.h>
#include <p4t/model/chain.h>
#include <p4t/model/filter.h>
#include <p4t/opt/oi_lpm_algos/common.h>
#include <p4t/transformations/transformations.h>

namespace p4t::opt::oi_lpm_algos {

namespace transformation_utils {

template <model::HasMask T>
struct details {
    using TransformedValues = Transformed<T>;

    static auto filter_for(
            TransformedValues filters, model::Filter::BitArray base,
            vector<T> const &others) -> TransformedValues;

    static void remove_non_oi(TransformedValues &filters);

    static void
    filter_intersecting(TransformedValues &filters, vector<T> const &others);

    static void intersect_all_with(
            TransformedValues &filters, model::Filter::BitArray base);

    static auto filter_for(
            TransformedValues filters,
            model::Chain<model::ChainKind::DESCENDING> chain)
            -> TransformedValues;

    static auto reduce_to_max_size(
            TransformedValues filters, model::Filter::BitArray base,
            int max_support_size) -> TransformedValues;

    static auto reduce_width(vector<T> xs, int max_support_size)
            -> transformations::SubsetWithCommonMask;
};

template <class T>
auto filter_for(
        Transformed<T> filters, model::Filter::BitArray base,
        vector<T> const &others = {}) -> Transformed<T> {
    return details<T>::filter_for(filters, base, others);
}

template <model::HasMask T>
void remove_non_oi(Transformed<T> &filters) {
    return details<T>::remove_non_oi(filters);
}

template <model::HasMask T>
void filter_intersecting(Transformed<T> &filters, vector<T> const &others) {
    details<T>::filter_intersecting(filters, others);
}

template <class T>
void intersect_all_with(Transformed<T> &filters, model::Filter::BitArray base) {
    details<T>::intersect_all_with(filters, base);
}

template <class T>
auto filter_for(
        Transformed<T> filters,
        model::Chain<model::ChainKind::DESCENDING> chain) -> Transformed<T> {
    return details<T>::filter_for(std::move(filters), chain);
}

template <class T>
auto reduce_width(vector<T> xs, int max_support_size)
        -> transformations::SubsetWithCommonMask {
    return details<T>::reduce_width(std::move(xs), max_support_size);
}

template <class T>
auto reduce_to_max_size(
        Transformed<T> filters, model::Filter::BitArray base,
        int max_support_size) -> Transformed<T> {
    return details<T>::reduce_to_max_size(filters, base, max_support_size);
}

extern template struct details<model::Filter>;
extern template struct details<model::Rule>;
} // namespace transformation_utils

using namespace transformation_utils;

} // namespace p4t::opt::oi_lpm_algos
#endif
