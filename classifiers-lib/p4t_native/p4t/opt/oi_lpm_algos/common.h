#ifndef OI_LPM_ALGOS_COMMON_H
#define OI_LPM_ALGOS_COMMON_H

#include <p4t/model/chain.h>
#include <p4t/model/filter.h>
#include <p4t/model/rule.h>
#include <p4t/transformations/transformations.h>

#include <unordered_map>
#include <unordered_set>

namespace p4t::opt::oi_lpm_algos {

template <class T>
using Transformed = transformations::SubsetWithMask::Transformed<T>;

using OiLpmResult = std::pair<
        transformations::SubsetWithMask,
        model::Chain<model::ChainKind::DESCENDING>>;

using support_set = std::unordered_set<model::Filter::BitArray>;

template <class T>
using support_map = std::unordered_map<model::Filter::BitArray, T>;

namespace common {
template <class T>
struct details {
    static auto get_max_cover_size(vector<T> const &xs) -> int;

    static auto get_maximal_supports(
            vector<T> const &filters,
            std::optional<int> max_support_size = std::nullopt)
            -> std::vector<model::Filter::BitArray>;

    static auto to_support_set(vector<T> const &xs) -> support_set;

    template <model::ChainKind Kind>
    struct chain_details {
        static void verify_chain(vector<T> const &xs);
    };

    template <model::ChainKind Kind>
    static void verify_chain(vector<T> const &xs) {
        chain_details<Kind>::verify_chain(xs);
    }
};

template <model::HasMask T>
auto get_max_cover_size(vector<T> const &xs) -> int {
    return details<T>::get_max_cover_size(xs);
}

template <model::HasMask T>
auto get_maximal_supports(
        vector<T> const &xs, optional<int> max_support_size = std::nullopt)
        -> std::vector<model::Filter::BitArray> {
    return details<T>::get_maximal_supports(xs, max_support_size);
}

template <model::HasMask T>
auto to_support_set(vector<T> const &xs) -> support_set {
    return details<T>::to_support_set(xs);
}

template <model::ChainKind Kind, model::HasMask T>
void verify_chain(vector<T> const &xs) {
    details<T>::template verify_chain<Kind>(xs);
}

extern template struct details<model::Rule>::chain_details<
        model::ChainKind::DESCENDING>;
extern template struct details<model::Rule>::chain_details<
        model::ChainKind::ASCENDING>;
extern template struct details<model::Rule>;
extern template struct details<model::Filter>::chain_details<
        model::ChainKind::ASCENDING>;
extern template struct details<model::Filter>::chain_details<
        model::ChainKind::DESCENDING>;
extern template struct details<model::Filter>;

} // namespace common

using namespace common;

auto filter_supports_by_size(support_set const &supports, int max_support_size)
        -> support_set;

auto check_the_same(
        vector<model::Filter> const &xs, vector<model::Filter> const &ys)
        -> bool;

} // namespace p4t::opt::oi_lpm_algos

#endif
