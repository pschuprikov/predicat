#ifndef INCREMENTAL_OI_LPM_H
#define INCREMENTAL_OI_LPM_H

#include <p4t/opt/oi_lpm_algos/common.h>

namespace p4t::opt::oi_lpm_algos {

namespace incremental_oi_lpm {

enum class CheckCanAddDecisionSource { QUICK, COMLETE_RECHECK };

template <class T>
struct details {
    static auto check_if_oi_lpm(vector<T> xs) -> std::optional<OiLpmResult>;

    static auto check_can_add(vector<T> xs, T x, OiLpmResult previous_result)
            -> pair<CheckCanAddDecisionSource, optional<OiLpmResult>>;

    static auto find_oi_lpm_incremental(vector<T> const &xs)
            -> oi_lpm_algos::OiLpmResult;

    static auto
    find_unique_for_mask(vector<T> elems, model::Filter::BitArray mask)
            -> vector<int>;
};

template <class T>
requires model::HasMask<T> &&model::HasValue<T> auto
check_if_oi_lpm(vector<T> xs) -> std::optional<OiLpmResult> {
    return details<T>::check_if_oi_lpm(xs);
}

template <class T>
requires model::HasMask<T> &&model::HasValue<T> auto
check_can_add(vector<T> xs, T x, OiLpmResult previous_result = OiLpmResult{})
        -> pair<CheckCanAddDecisionSource, optional<OiLpmResult>> {
    return details<T>::check_can_add(xs, x, previous_result);
}

template <class T>
requires model::HasMask<T> &&model::HasValue<T> auto
find_oi_lpm_incremental(vector<T> const &xs) -> oi_lpm_algos::OiLpmResult {
    return details<T>::find_oi_lpm_incremental(xs);
}

template <class T>
requires model::HasMask<T> &&model::HasValue<T> auto
find_unique_for_mask(vector<T> elems, model::Filter::BitArray mask)
        -> vector<int> {
    return details<T>::find_unique_for_mask(elems, mask);
}

extern template struct details<model::Filter>;
extern template struct details<model::Rule>;
} // namespace incremental_oi_lpm

using namespace incremental_oi_lpm;

} // namespace p4t::opt::oi_lpm_algos

#endif
