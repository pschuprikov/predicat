#include "common.h"

#include <p4t/model/rule.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/chain_algos.h>
#include <p4t/opt/chain_basic_algos.hpp>
#include <p4t/utils/timer.h>

namespace {

using namespace p4t;
using namespace opt;
using namespace oi_lpm_algos;

template <model::ChainKind Kind, model::HasMask T>
auto find_chain_violation(vector<T> const &filters) {
    return std::adjacent_find(begin(filters), end(filters), [](auto a, auto b) {
        if constexpr (Kind == model::ChainKind::ASCENDING) {
            return (a.mask() & b.mask()) != a.mask();
        } else {
            static_assert(Kind == model::ChainKind::DESCENDING);
            return (a.mask() & b.mask()) != b.mask();
        }
    });
}

template <model::ChainKind Kind, model::HasMask T>
auto is_filter_chain(vector<T> const &filters) {
    return find_chain_violation<Kind>(filters) == end(filters);
}

} // namespace

namespace p4t::opt {

namespace oi_lpm_algos::common {

template <class T>
auto details<T>::get_max_cover_size(vector<T> const &filters) -> int {
    utils::Timer t("get_max_cover_size");
    auto supports = to_supports(filters);
    auto [unique_supports, weights] = model::select_unique_n_weight(supports);
    auto const partitions = opt::find_min_bounded_chain_partition(
            {unique_supports}, {weights}, 1);
    if (!(partitions.size() == 1)) {
        abort();
    }
    auto cover = 0;
    for (auto chain : partitions.front()) {
        for (auto support : chain) {
            auto it = std::find(
                    begin(unique_supports), end(unique_supports), support);
            if (it == end(unique_supports)) {
                abort();
            }
            cover += weights[it - begin(unique_supports)];
        }
    }
    return cover;
}

template <class T>
auto details<T>::get_maximal_supports(
        vector<T> const &xs, std::optional<int> max_support_size)
        -> std::vector<model::Filter::BitArray> {
    utils::Timer t("get_maximal_supports");
    auto const supports = to_support_set(xs);
    auto filtered_supports =
            max_support_size
                    ? filter_supports_by_size(supports, *max_support_size)
                    : supports;
    auto const cmp = [](auto lhs, auto rhs) { return (lhs & rhs) == lhs; };
    auto const maximal_elements = split_maximal_elements<support_set>(
                                          begin(supports), end(supports), cmp)
                                          .first;
    return std::vector(begin(maximal_elements), end(maximal_elements));
}

template <class T>
auto details<T>::to_support_set(vector<T> const &filters) -> support_set {
    support_set supports;
    std::transform(
            begin(filters), end(filters),
            std::inserter(supports, end(supports)),
            [](auto const &f) { return f.mask(); });
    return supports;
}

template<class T> 
template <model::ChainKind Kind>
void details<T>::chain_details<Kind>::verify_chain(vector<T> const &filters) {
    if (!is_filter_chain<Kind>(filters)) {
        auto const viol_it = find_chain_violation<Kind>(filters);
        log()->critical("not a chain: {} {}", *viol_it, *(viol_it + 1));
        throw std::logic_error("filter sequence is not a chain");
    }
}

template struct details<model::Rule>;
template struct details<model::Rule>::chain_details<model::ChainKind::ASCENDING>;
template struct details<model::Rule>::chain_details<model::ChainKind::DESCENDING>;
template struct details<model::Filter>;
template struct details<model::Filter>::chain_details<model::ChainKind::ASCENDING>;
template struct details<model::Filter>::chain_details<model::ChainKind::DESCENDING>;

}

auto oi_lpm_algos::filter_supports_by_size(
        support_set const &supports, int max_support_size) -> support_set {
    support_set result{};
    std::copy_if(
            begin(supports), end(supports), std::inserter(result, end(result)),
            [max_support_size](auto const &s) {
                return is_zero(s) || int(popcount(s)) <= max_support_size;
            });
    return result;
}

auto oi_lpm_algos::check_the_same(
        vector<model::Filter> const &xs, vector<model::Filter> const &ys)
        -> bool {
    if (xs.size() != ys.size()) {
        log()->error("different sizes: {:d} vs {:d}", xs.size(), ys.size());
        return false;
    }
    return xs == ys;
}


} // namespace p4t::opt
