#include "transformation_utils.h"

#include <p4t/model/filter.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/oi_algos.h>
#include <p4t/transformations/transformations.hpp>
#include <p4t/utils/timer.h>

namespace {

using namespace p4t;

// TODO: merge with oi_algos.cpp
auto filter(model::Filter const &filter) { return filter; }

auto filter(model::Rule const &rule) { return rule.filter(); }

template <class T>
concept HasFilter = requires(T const &x) {
    { filter(x) }
    ->std::same_as<model::Filter>;
};

} // namespace

namespace p4t::opt::oi_lpm_algos::transformation_utils {

template <model::HasMask T>
void details<T>::filter_intersecting(
        TransformedValues &filters, vector<T> const &others) {
    filters.remove_if([&others](auto const &u) {
        return find_if(begin(others), end(others), [u](auto const &v) {
                   return model::Filter::subsums(filter(u), filter(v)) &&
                          T::intersect(u, v);
               }) != end(others);
    });
}

template <model::HasMask T>
void details<T>::remove_non_oi(TransformedValues &filters) {
    filters.sort([](auto const &lhs, auto const &rhs) {
        return popcount(lhs.mask()) > popcount(rhs.mask());
    });

    filters.remove_forward_if([](auto &lhs, auto &rhs) {
        return model::Filter::subsums(filter(rhs), filter(lhs)) &&
               T::intersect(rhs, lhs);
    });
}

template <model::HasMask T>
auto details<T>::filter_for(
        Transformed<T> filters, model::Filter::BitArray base,
        vector<T> const &others) -> Transformed<T> {
    utils::Timer t("filter_for");
    intersect_all_with(filters, base);
    remove_non_oi(filters);
    filter_intersecting(filters, others);
    return filters;
}

template <model::HasMask T>
void details<T>::intersect_all_with(
        TransformedValues &filters, model::Filter::BitArray base) {
    filters.apply([base](auto &f) { f = f & base; });
}

template <model::HasMask T>
auto details<T>::filter_for(
        Transformed<T> filters,
        model::Chain<model::ChainKind::DESCENDING> chain) -> Transformed<T> {
    TransformedValues result;
    for (auto const &support : chain.get_chain()) {
        auto const &mask = model::to_mask(support);
        transformation_utils::filter_for(
                filters, mask, to_vector(result.filters()));
        result.add_all(filters.extract_if(
                [mask](auto const &f) { return f.mask() == mask; }));
    }
    return result;
}

template <model::HasMask T>
auto details<T>::reduce_to_max_size(
        Transformed<T> filters, model::Filter::BitArray base,
        int max_support_size) -> Transformed<T> {
    filters.remove_if([base](auto const &f) {
        return !((f.mask() != base) && (f.mask() & base) == base);
    });
    auto const oi_transform = best_to_stay_minme(
            to_vector(filters.filters()),
            {.l = static_cast<size_t>(max_support_size),
             .mode = MinMEMode::BLOCKERS,
             .oi_mode = OIMode::TOP_DOWN,
             .only_exact = false,
             .bit_ranges = {{
                     .lower = model::to_support(base),
             }}});
    filters.chain(oi_transform);
    return filters;
}

template <model::HasMask T>
auto details<T>::reduce_width(vector<T> xs, int max_support_size)
        -> transformations::SubsetWithCommonMask {
    return best_to_stay_minme(
            std::move(xs), {.l = static_cast<size_t>(max_support_size),
                            .mode = MinMEMode::BLOCKERS,
                            .oi_mode = OIMode::TOP_DOWN});
}

template struct details<model::Filter>;
template struct details<model::Rule>;

} // namespace p4t::opt::oi_lpm_algos::transformation_utils
