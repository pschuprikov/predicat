#include "greedy_all.h"

#include <algorithm>
#include <bits/ranges_algo.h>
#include <p4t/model/filter.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/oi_algos.h>
#include <p4t/opt/oi_lpm_algos/common.h>
#include <p4t/opt/oi_lpm_algos/transformation_utils.h>
#include <p4t/transformations/transformations.hpp>

#include <execution>
#include <ranges>

namespace {

using namespace p4t;
using namespace opt;
using namespace oi_lpm_algos;

template <class T>
struct GreedyOptimizer {
    GreedyOptimizer(
            vector<T> const &filters, std::optional<int> max_support_size,
            GreedyStrategy strategy)
        : strategy{strategy}, max_support_size{max_support_size},
          remaining_filters(filters), result{}, chain{} {}

    void optimize() {
        while (!remaining_filters.empty()) {
            optimize_next();
        }
    }

    auto get_previous_filters_for(model::Filter::BitArray const &sup) const {
        if (!max_support_size || result.empty()) {
            return result;
        }

        if (int(popcount(sup)) >= *max_support_size) {
            return filter_for(result, sup);
        }

        auto reduced_result = result;
        reduce_to_max_size(reduced_result, sup, *max_support_size);
        return reduced_result;
    }

    struct Choice {
        model::Filter::BitArray support;
        Transformed<T> before;
        Transformed<T> after;
    };

    void optimize_next() {
        auto const maximal_supports =
                get_maximal_supports(to_vector(remaining_filters.filters()));

        log()->info(
                "number of max elements: {:d}; max elements size: {:d}; remaining_filters: {:d}",
                maximal_supports.size(), 
                get_max_size(maximal_supports), 
                remaining_filters.size());

        auto const choices = get_choices(maximal_supports);

        auto [best_value, best_choice] = get_best_choice(choices);

        result = best_choice.before;
        remaining_filters = best_choice.after;

        auto const filters_to_add = remaining_filters.extract_if(
                [best_choice=best_choice](auto const &f) {
                    return f.mask() == best_choice.support;
                });

        if (max_support_size &&
            int(popcount(best_choice.support)) >= *max_support_size &&
            chain.empty() && !result.empty()) {
            chain = {model::to_support(result.filters().begin()->mask())};
        }

        chain.push_back(model::to_support(best_choice.support));

        if (!is_zero(best_choice.support)) {
            result.add_all(filters_to_add);
        }

        log()->info(
                "max cover: {:d}, count so far: {:d}", best_value,
                result.size());
    }


    auto const &get_result() const { return result; }

    auto const &get_chain() const { return chain; }

  private:
    auto get_choices(
            vector<model::Filter::BitArray> const& maximal_supports
            ) const -> vector<Choice> {
        auto choices = std::vector<Choice>(maximal_supports.size());
        std::ranges::transform(
                maximal_supports, begin(choices), [](auto const &support) {
                    return Choice{.support = support, .before{}, .after{}};
                });

        std::for_each(
                std::execution::par, begin(choices), end(choices),
                [this](Choice &choice) {
                    choice.before = get_previous_filters_for(choice.support);
                    choice.after = filter_for(
                            remaining_filters, choice.support,
                            to_vector(choice.before.filters()));
                });
        return choices;
    }

    auto get_best_choice(
            std::vector<Choice> const& choices
            ) const -> std::tuple<int, Choice> {
        auto values = std::vector<int>(choices.size());

        std::transform(
                std::execution::par, begin(choices), end(choices),
                begin(values), get_strategy_function());

        auto const best_value_it = std::max_element(begin(values), end(values));
        auto const &best_choice =
                choices[std::distance(begin(values), best_value_it)];
        return {*best_value_it, best_choice};
    }

    auto get_strategy_function() const -> std::function<int(Choice const &)> {
        switch (strategy) {
        case GreedyStrategy::MAX_LPM:
            return lpm_value;
        case GreedyStrategy::MAX_OI:
            return oi_value;
        case GreedyStrategy::MAX_STEP:
            return step_value;
        }
    }

  private:
    static auto get_max_size(vector<model::Filter::BitArray> const& supports) {
        return popcount(*std::max_element(
                begin(supports), end(supports),
                [](auto const& a, auto const& b) {
                    return popcount(a) < popcount(b);
                }));
    }

    static auto lpm_value(Choice const &choice) -> int {
        return get_max_cover_size(to_vector(choice.after.filters())) +
               choice.before.size();
    }

    static auto oi_value(Choice const &choice) -> int {
        auto const &xs = choice.after.filters();
        return find_maximal_oi_subset(
                       vector(ranges::begin(xs), ranges::end(xs)),
                       choice.support, OIMode::TOP_DOWN)
                       .size() +
               choice.before.size();
    }

    static auto step_value(Choice const &choice) -> int {
        return std::ranges::count_if(
                       choice.after.filters(),
                       [&choice](auto const &f) {
                           return f.mask() == choice.support;
                       }) +
               choice.before.size();
    }

  private:
    GreedyStrategy const strategy;
    std::optional<int> const max_support_size;

    Transformed<T> remaining_filters;
    Transformed<T> result;
    model::Chain<model::ChainKind::DESCENDING> chain;
};

template <class T>
auto find_oi_lpm_subset_greedy_all_aux(
        vector<T> const &filters, std::optional<int> max_support_size,
        GreedyStrategy strategy) -> OiLpmResult {
    auto optimizer = GreedyOptimizer{filters, max_support_size, strategy};

    optimizer.optimize();

    return make_pair(
            optimizer.get_result().transformation(), optimizer.get_chain());
}

} // namespace

namespace p4t::opt {

auto oi_lpm_algos::find_oi_lpm_subset_greedy_all(
        vector<model::Filter> const &filters,
        std::optional<int> max_support_size, GreedyStrategy strategy)
        -> OiLpmResult {
    return find_oi_lpm_subset_greedy_all_aux(
            filters, max_support_size, strategy);
}

auto oi_lpm_algos::find_oi_lpm_subset_greedy_all(
        vector<model::Rule> const &rules, std::optional<int> max_support_size,
        GreedyStrategy strategy) -> OiLpmResult {
    return find_oi_lpm_subset_greedy_all_aux(rules, max_support_size, strategy);
}

} // namespace p4t::opt
