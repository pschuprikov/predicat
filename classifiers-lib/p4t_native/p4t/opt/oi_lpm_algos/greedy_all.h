#ifndef OI_LPM_ALGOS_GREEDY_ALL_H
#define OI_LPM_ALGOS_GREEDY_ALL_H

#include <p4t/model/chain.h>
#include <p4t/model/filter.h>
#include <p4t/opt/oi_lpm_algos/common.h>
#include <p4t/transformations/transformations.h>

namespace p4t::opt::oi_lpm_algos {

enum class GreedyStrategy { MAX_LPM, MAX_OI, MAX_STEP };

auto find_oi_lpm_subset_greedy_all(
        vector<model::Filter> const &filters,
        std::optional<int> max_support_size = std::nullopt,
        GreedyStrategy strategy = GreedyStrategy::MAX_LPM) -> OiLpmResult;

auto find_oi_lpm_subset_greedy_all(
        vector<model::Rule> const &rules,
        std::optional<int> max_support_size = std::nullopt,
        GreedyStrategy strategy = GreedyStrategy::MAX_LPM) -> OiLpmResult;

} // namespace p4t::opt::oi_lpm_algos

#endif
