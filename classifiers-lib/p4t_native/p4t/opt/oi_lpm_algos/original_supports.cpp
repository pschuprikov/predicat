#include "original_supports.h"

#include <p4t/model/filter.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/oi_lpm_algos/common.h>
#include <p4t/opt/oi_lpm_algos/transformation_utils.h>
#include <p4t/transformations/transformations.h>
#include <p4t/transformations/transformations.hpp>

#include <algorithm>
#include <optional>

using namespace p4t::opt::oi_lpm_algos;
using namespace p4t::model;
using namespace p4t::transformations;

namespace {

struct State {
    std::optional<Filter::BitArray> previous;
    int num_rules;
};

} // namespace

using DPState = std::unordered_map<Filter::BitArray, State>;

namespace {

auto recover_chain_for(Filter::BitArray const &mask, DPState const &st) {
    Chain<ChainKind::ASCENDING> result{};
    auto cur_mask = std::optional(mask);
    while (cur_mask) {
        result.push_back(to_support(*cur_mask));
        cur_mask = st.at(*cur_mask).previous;
    }
    return result.reversed();
}

} // namespace

namespace p4t::opt {

auto oi_lpm_algos::find_oi_lpm_subset_original_supports(
        [[maybe_unused]] vector<model::Filter> const &filters,
        [[maybe_unused]] std::optional<int> max_support_size)
        -> std::pair<
                transformations::SubsetWithMask,
                model::Chain<model::ChainKind::DESCENDING>> {
    auto const supports = to_support_set(filters);
    auto supports_size_desc = vector(begin(supports), end(supports));

    std::sort(
            begin(supports_size_desc), end(supports_size_desc),
            [](auto const &a, auto const &b) {
                return popcount(a) > popcount(b);
            });

    auto const dp = std::unordered_map<model::Filter::BitArray, State>{};

    for (auto it = begin(supports_size_desc); it != end(supports_size_desc);
         ++it) {
        auto bestState = State{
                .previous = std::nullopt,
                .num_rules = static_cast<int>(
                        ::filter_for(
                                SubsetWithMask::Transformed{filters},
                                Chain<ChainKind::DESCENDING>{to_support(*it)})
                                .size())};
        for (auto prev_it = begin(supports_size_desc); prev_it != it;
             prev_it++) {
            if ((*prev_it & *it) != *it) {
                continue;
            }
            auto chain = recover_chain_for(*prev_it, dp);
            chain.push_back(to_support(*it));
            auto const newState = State{
                    .previous = *prev_it,
                    .num_rules = static_cast<int>(
                            filter_for(
                                    SubsetWithMask::Transformed(filters), chain)
                                    .size())};
            if (newState.num_rules > bestState.num_rules) {
                bestState = newState;
            }
        }
    }

    auto const best_of_the_best = std::max_element(
            begin(dp), end(dp), [](auto const &a, auto const &b) {
                return a.second.num_rules < b.second.num_rules;
            });
    auto const best_chain = recover_chain_for(best_of_the_best->first, dp);

    return make_pair(
            filter_for(SubsetWithMask::Transformed(filters), best_chain)
                    .transformation(),
            best_chain);
}

auto oi_lpm_algos::find_oi_lpm_subset_original_supports(
        [[maybe_unused]] vector<model::Rule> const &rules,
        [[maybe_unused]] std::optional<int> max_support_size)
        -> std::pair<
                transformations::SubsetWithMask,
                model::Chain<model::ChainKind::DESCENDING>> {
    throw std::logic_error("not implemented yet");
}

} // namespace p4t::opt
