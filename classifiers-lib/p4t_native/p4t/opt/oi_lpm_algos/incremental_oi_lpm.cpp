#include "incremental_oi_lpm.h"

#include <p4t/model/filter.hpp>
#include <p4t/model/rule.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/oi_lpm_algos/common.h>
#include <p4t/transformations/transformations.hpp>

#include <algorithm>
#include <execution>
#include <functional>
#include <numeric>

using namespace p4t;
using namespace model;
using namespace opt;
using namespace oi_lpm_algos;

using Mask = Filter::BitArray;
using Value = Filter::BitArray;
using ValueSet = oi_lpm_algos::support_set;

namespace {

template <class T>
auto get_intersection_mask(vector<T> const &xs) -> Mask {
    return std::transform_reduce(
            begin(xs), end(xs), Filter::BitArray::ones(), std::bit_and(),
            [](auto const &x) { return x.mask(); });
}

template <class T>
auto intersects_with(T const &x, vector<T> const &xs) -> bool {
    return std::find_if(begin(xs), end(xs), [x](auto const &y) {
               return T::intersect(x, y);
           }) != end(xs);
}

template <class T>
class OiLpmChecker {
    OiLpmChecker(
            Transformed<T> current, Transformed<T> result,
            model::Chain<model::ChainKind::ASCENDING> chain)
        : current_(std::move(current)), result_(std::move(result)),
          chain_(std::move(chain)) {}

  public:
    explicit OiLpmChecker(vector<T> xs) : current_(xs), result_{}, chain_{} {}

    auto check() -> std::optional<OiLpmResult> {
        while (!current_.empty()) {
            if (!step()) {
                return std::nullopt;
            }
        }

        result_.reverse();

        return std::make_pair(result_.transformation(), chain_.reversed());
    }

  private:
    auto step() -> bool {
        auto const &current_xs = to_vector(current_.filters());

        auto const mask = get_intersection_mask(current_xs);

        auto const good_masks = find_unique_for_mask(current_xs, mask);

        auto more_result = current_.extract(good_masks);
        more_result.apply([mask](auto &x) { x = x & mask; });

        if (more_result.empty()) {
            return false;
        }
        chain_.push_back(to_support(mask));
        result_.add_all(more_result);
        return true;
    }

  private:
    Transformed<T> current_;
    Transformed<T> result_;
    model::Chain<model::ChainKind::ASCENDING> chain_;
};

} // namespace

namespace p4t::opt::oi_lpm_algos::incremental_oi_lpm {

template <class T>
auto details<T>::check_if_oi_lpm(vector<T> xs) -> std::optional<OiLpmResult> {
    return OiLpmChecker<T>(xs).check();
}

template <class T>
auto details<T>::find_oi_lpm_incremental(vector<T> const &xs) -> OiLpmResult {
    auto const rules_total = xs.size();
    auto filters_transformed = Transformed<T>(xs);
    auto result = Transformed<T>{};
    filters_transformed.sort([](auto const &x, auto const &y) {
        return popcount(x.mask()) < popcount(y.mask());
    });

    auto last_mask = Mask::ones();
    auto previous_result = OiLpmResult{};

    auto constexpr const REPORT_PERIOD = 100;
    auto iter_count = 0;
    auto quick_count = 0;
    auto last_time = std::clock();
    while (!filters_transformed.empty()) {
        if ((iter_count % REPORT_PERIOD) == 0) {
            log()->info(
                    "incremental: {:d} rules remain; {:d}/{:d} added "
                    "({:.2f} ms/rule, {:d} quick, {:d} isec bits)",
                    filters_transformed.size(), result.size(),
                    rules_total - filters_transformed.size(),
                    (std::clock() - last_time) * 1000.0 / CLOCKS_PER_SEC /
                            REPORT_PERIOD,
                    quick_count,
                    popcount(last_mask & filters_transformed.back().mask()));
            last_time = std::clock();
            quick_count = 0;
            iter_count = 0;
        }

        iter_count++;

        auto const candidate = filters_transformed.pop_back();

        auto const [src, cur_result_opt] = check_can_add(
                to_vector(result.filters()), candidate.element,
                previous_result);
        if (src == CheckCanAddDecisionSource::QUICK) {
            quick_count++;
        }

        if (!cur_result_opt) {
            continue;
        }

        if (cur_result_opt->second.empty()) {
            throw std::logic_error("weird");
        }

        if (to_mask(cur_result_opt->second.get_chain().back()) != last_mask) {
            last_mask = to_mask(cur_result_opt->second.get_chain().back());
            filters_transformed.sort([last_mask](auto const &x, auto const &y) {
                return popcount(x.mask() & last_mask) <
                       popcount(y.mask() & last_mask);
            });
        }

        result.push_back(candidate);
        previous_result = *cur_result_opt;
    }

    auto [trans, chain] = check_if_oi_lpm(to_vector(result.filters())).value();
    result.chain(trans);
    return {result.transformation(), chain};
}

template <class T>
auto details<T>::find_unique_for_mask(vector<T> elems, Mask mask)
        -> vector<int> {
    std::for_each(begin(elems), end(elems), [mask](auto &x) { x = x & mask; });

    vector<int> values(elems.size());
    std::iota(begin(values), end(values), 0);

    std::sort(
            std::execution::par, begin(values), end(values),
            [&elems](auto i, auto j) {
                return elems[i].value() < elems[j].value();
            });

    std::vector<int> result;
    for (auto it = begin(values); it != end(values); ++it) {
        auto const cur = elems[*it];
        auto next_it = std::find_if(it, end(values), [cur, &elems](auto j) {
            return elems[j].value() != cur.value();
        });
        auto isect_it = std::find_if(it + 1, next_it, [cur, &elems](auto j) {
            return T::intersect(elems[j], cur);
        });

        if (isect_it == next_it) {
            result.push_back(*it);
        }
        it = next_it - 1;
    }
    std::sort(std::execution::par, begin(result), end(result));
    return result;
}

template <class T>
auto details<T>::check_can_add(vector<T> xs, T x, OiLpmResult previous_result)
        -> pair<CheckCanAddDecisionSource, optional<OiLpmResult>> {
    auto const ys = previous_result.first.apply(xs);

    // TODO: must be equal to intersection_mask
    auto const mask =
            previous_result.second.empty()
                    ? Mask::ones()
                    : to_mask(previous_result.second.get_chain().back());

    if (!intersects_with(x & mask, ys)) {
        previous_result.first.push_back(x.mask() & mask, xs.size());
        if ((x.mask() & mask) != mask) {
            previous_result.second.push_back(to_support(x.mask() & mask));
        }

        return {CheckCanAddDecisionSource::QUICK, previous_result};
    }
    if (intersects_with(x, xs)) {
        return {CheckCanAddDecisionSource::QUICK, std::nullopt};
    }

    xs.push_back(x);
    return {CheckCanAddDecisionSource::COMLETE_RECHECK, check_if_oi_lpm(xs)};
}

template struct details<model::Filter>;
template struct details<model::Rule>;
} // namespace p4t::opt::oi_lpm_algos::incremental_oi_lpm
