#ifndef CHAIN_BASIC_ALGOS_HPP
#define CHAIN_BASIC_ALGOS_HPP

#include <p4t/common.h>

namespace p4t::opt {
template <class ResultSetT, class Comparator, class ForwardIt>
auto split_maximal_elements(ForwardIt first, ForwardIt beyond, Comparator comp)
        -> std::pair<ResultSetT, ResultSetT> {
    auto result = ResultSetT{};
    auto rest = ResultSetT{};
    for (auto it = first; it != beyond; ++it) {
        auto good = true;
        for (auto x : vector(begin(result), end(result))) {
            if (comp(x, *it)) {
                result.erase(x);
                rest.insert(x);
            }
            if (comp(*it, x)) {
                good = false;
                break;
            }
        }
        if (good) {
            result.insert(*it);
        } else {
            rest.insert(*it);
        }
    }
    return make_pair(result, rest);
}

} // namespace p4t::opt

#endif
