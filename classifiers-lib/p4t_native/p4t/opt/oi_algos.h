#ifndef OI_ALGOS_H
#define OI_ALGOS_H

#include <p4t/common.h>
#include <p4t/model/filter.h>
#include <p4t/model/rule.h>
#include <p4t/transformations/transformations.h>

namespace p4t::opt {

template <class T>
concept Intersectable =
        requires(T const &x, model::Filter::BitArray const &mask) {
    { T::intersect(x, x, mask) }
    ->std::same_as<bool>;
};

enum MinMEMode { MAX_OI, BLOCKERS };

enum OIMode { TOP_DOWN, MIN_DEGREE };

struct BitStats {
    explicit BitStats(size_t n)
        : dontcare(n), zeros(n), ones(n), exact_bits{} {}

    vector<int> dontcare;
    vector<int> zeros;
    vector<int> ones;
    vector<int> exact_bits;
};

struct BitRange {
    std::optional<model::Support> lower;
    std::optional<model::Support> upper;
};

auto calc_bit_stats(vector<model::Filter> const &filters) -> BitStats;

auto best_min_similarity_bits(vector<model::Filter> const &filters, size_t l)
        -> vector<int>;

struct OIParams {
    size_t l;
    MinMEMode mode;
    OIMode oi_mode;
    bool only_exact = false;
    vector<BitRange> bit_ranges = {BitRange{}};
};

auto best_to_stay_minme(vector<model::Filter> filters, OIParams params)
        -> transformations::SubsetWithCommonMask;

auto best_to_stay_minme(vector<model::Rule> rules, OIParams params)
        -> transformations::SubsetWithCommonMask;

template <Intersectable T>
auto find_maximal_oi_subset(
        vector<T> const &xs, model::Filter::BitArray const &mask, OIMode mode)
        -> vector<int>;

auto bits_to_mask(vector<int> const &bits) -> model::Filter::BitArray;

template <Intersectable T> auto is_oi(vector<T> const &xs) -> bool;

template <Intersectable T> void verify_oi(vector<T> const &xs);

extern template auto find_maximal_oi_subset(
        vector<model::Rule> const &, model::Filter::BitArray const &, OIMode)
        -> vector<int>;
extern template auto find_maximal_oi_subset(
        vector<model::Filter> const &, model::Filter::BitArray const &, OIMode)
        -> vector<int>;

extern template auto is_oi(vector<model::Filter> const &) -> bool;
extern template auto is_oi(vector<model::Rule> const &) -> bool;

extern template void verify_oi(vector<model::Filter> const &);
extern template void verify_oi(vector<model::Rule> const &);

} // namespace p4t::opt

#endif
