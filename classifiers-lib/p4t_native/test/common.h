#include <p4t/model/filter.hpp>
#include <p4t/model/rule.hpp>

using namespace p4t;
using namespace model;

inline auto mk_filter(std::string_view chars) {
    auto result = Filter(chars.size());
    for (auto it = begin(chars); it != end(chars); it++) {
        switch (*it) {
        case '*':
            result.set(it - begin(chars), Bit::ANY);
            break;
        case '0':
            result.set(it - begin(chars), Bit::ZERO);
            break;
        case '1':
            result.set(it - begin(chars), Bit::ONE);
            break;
        }
    }
    return result;
}

inline auto mk_rule(std::pair<std::string_view, int> filter_action) {
    return Rule(mk_filter(filter_action.first), Action(filter_action.second));
}
