#include <catch2/catch.hpp>

#include <test/common.h>

#include <p4t/model/chain.h>
#include <p4t/model/filter.hpp>
#include <p4t/model/support.hpp>
#include <p4t/opt/oi_lpm_algos/incremental_oi_lpm.h>
#include <p4t/opt/oi_lpm_algos/transformation_utils.h>
#include <p4t/transformations/transformations.hpp>

namespace Catch {
template <>
struct StringMaker<p4t::model::Filter> {
    static std::string convert(p4t::model::Filter const &value) {
        return fmt::format("{}", value);
    }
};
} // namespace Catch

namespace {

using namespace p4t;
using namespace model;
using TransformedFilters =
        transformations::SubsetWithMask::Transformed<model::Filter>;

auto build_classifier(std::vector<std::string_view> rules) {
    auto result = vector<Filter>(rules.size(), Filter{});
    std::transform(begin(rules), end(rules), begin(result), mk_filter);
    return result;
}

} // namespace

using namespace p4t::opt::oi_lpm_algos;

TEST_CASE("Empty chain produces empty result", "[filter_for]") {
    auto example = TransformedFilters{build_classifier({})};
    REQUIRE(filter_for(example, Chain<ChainKind::DESCENDING>{}).empty());
}

TEST_CASE("Non-empty single mask chain filters correctly", "[filter_for]") {
    auto example = TransformedFilters{build_classifier({"*01", "*11", "111"})};
    auto const chain = Chain<ChainKind::DESCENDING>{{0, 1, 2}};
    auto expected = build_classifier({
            "111",
    });
    REQUIRE(to_vector(filter_for(example, chain).filters()) == expected);
}

TEST_CASE("Extract extracts correctly one", "[filters.extract_if()]") {
    auto example = TransformedFilters{build_classifier({
            "111",
            "*01",
    })};
    auto const mask = to_mask({0, 1, 2});
    auto expected = build_classifier({
            "111",
    });
    REQUIRE(to_vector(example.extract_if([mask](auto const &f) {
                                 return f.mask() == mask;
                             })
                              .filters()) == expected);
}

TEST_CASE("Extract extracts correctly several", "[filters.extract_if()]") {
    auto example = TransformedFilters{build_classifier({
            "111",
            "011",
            "*01",
    })};
    auto const mask = to_mask({0, 1, 2});
    auto const expected = build_classifier({
            "111",
            "011",
    });
    auto const expected_remains = build_classifier({
            "*01",
    });
    auto const actual = example.extract_if([mask](auto const &f) {
            return f.mask() == mask;
    });
    REQUIRE(to_vector(actual.filters()) == expected);
    REQUIRE(to_vector(example.filters()) == expected_remains);
}

TEST_CASE("Extract leaves correctly several", "[filters.extract_if()]") {
    auto example = TransformedFilters{build_classifier({
            "110",
            "*01",
            "*11",
    })};
    auto const mask = to_mask({0, 1, 2});
    auto expected = build_classifier({
            "110",
    });
    auto expected_remains = build_classifier({
            "*01",
            "*11",
    });
    auto actual = example.extract_if([mask](auto const &f) {
            return f.mask() == mask;
    });
    REQUIRE(to_vector(actual.filters()) == expected);
    REQUIRE(to_vector(example.filters()) == expected_remains);
}


TEST_CASE("find unique indices works", "[find_unique_for_mask]") {
    auto const cls = build_classifier({
            "001",
            "011",
            "111",
    });
    auto indices = find_unique_for_mask(cls, to_mask({0, 1, 2}));
    REQUIRE(indices == vector{0, 1, 2});
}

TEST_CASE("Indexed extract extracts everything", "[filters.extract()]") {
    auto const cls = build_classifier({
            "001",
            "011",
            "111",
    });
    auto example = TransformedFilters{cls};
    auto extracted = example.extract({0, 1, 2});
    REQUIRE(example.empty());
    REQUIRE(to_vector(extracted.filters()) == cls);
}

TEST_CASE("filtering intersecting filters works", "[filter_intersecting]") {
    auto example = TransformedFilters{build_classifier({
            "*01",
            "*11",
    })};
    filter_intersecting(example, {});
    auto expected = build_classifier({
            "*01",
            "*11",
    });
    REQUIRE(to_vector(example.filters()) == expected);
}

TEST_CASE("remove non OI", "[remove_non_oi]") {
    auto example = TransformedFilters{build_classifier({
            "*01",
            "*11",
            "*11",
    })};
    remove_non_oi(example);
    auto expected = build_classifier({
            "*01",
            "*11",
    });
    REQUIRE(to_vector(example.filters()) == expected);
}

TEST_CASE("intersecting really works", "[intersect_all_with]") {
    auto example = TransformedFilters{build_classifier({
            "*01",
            "*11",
            "111",
    })};
    intersect_all_with(example, to_mask({1, 2}));
    auto expected = build_classifier({
            "*01",
            "*11",
            "*11",
    });
    REQUIRE(to_vector(example.filters()) == expected);
}

TEST_CASE("Non-empty mask filters correctly (1)", "[filter_for]") {
    auto example = TransformedFilters{build_classifier({
            "*01",
            "*11",
            "111",
    })};
    auto expected = build_classifier({
            "*01",
            "*11",
    });
    auto const result = filter_for(example, to_mask({1, 2}));

    REQUIRE(to_vector(result.filters()) == expected);
}

TEST_CASE("Full mask filters correctly", "[filter_for]") {
    auto example = TransformedFilters{build_classifier({
            "*01",
            "*11",
            "111",
    })};
    auto expected = build_classifier({
            "111",
            "*01",
    });
    auto const result = filter_for(example, to_mask({0, 1, 2}));

    REQUIRE(to_vector(result.filters()) == expected);
}

TEST_CASE("EXACT is OI and LPM", "[check_if_oi_lpm]") {
    auto example = build_classifier({
            "001",
            "011",
            "111",
    });
    REQUIRE(check_if_oi_lpm(example));
    REQUIRE(check_if_oi_lpm(example).value().second.size() == 1);
}

TEST_CASE("Mix can be OI and LPM", "[check_if_oi_lpm]") {
    auto example = build_classifier({
            "0*1",
            "10*",
            "111",
    });
    REQUIRE(check_if_oi_lpm(example));
}

TEST_CASE("LPM can be OI LPM", "[check_if_oi_lpm]") {
    auto example = build_classifier({
            "0**",
            "10*",
            "110",
    });
    REQUIRE(check_if_oi_lpm(example));
}

TEST_CASE("empty is OI LPM", "[check_if_oi_lpm]") {
    auto example = build_classifier({});
    REQUIRE(check_if_oi_lpm(example));
}
