cmake_minimum_required(VERSION 3.12)

project(p4t_native)

add_compile_options(-Wall -ggdb -Werror -Wextra -Wundefined-func-template -O3 -msse4.1)

include(CTest)
find_package(Python COMPONENTS Interpreter Development REQUIRED)
find_package(Threads REQUIRED)
find_package(spdlog REQUIRED)
find_package(Boost COMPONENTS python38 numpy38 REQUIRED)
find_package(Catch2 REQUIRED)
include_directories(${CMAKE_SOURCE_DIR})

if(NOT PYTHON_SITE_PACKAGES_DIR)
    set(PYTHON_SITE_PACKAGES_DIR ${Python_SITELIB})
endif()

add_subdirectory(p4t)
add_subdirectory(test)

install(TARGETS p4t_native DESTINATION ${PYTHON_SITE_PACKAGES_DIR})
install(TARGETS p4t_opt LIBRARY)
