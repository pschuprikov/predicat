from typing import Sequence, List, Optional, Tuple
from cls.classifiers.abstract import AbstractBasicClassifier
from cls.optimizations.oi_lpm import BitRange, BitChain


def min_bmgr(classifiers: Sequence[AbstractBasicClassifier], max_num_groups: int): ...

def min_pmgr(classifier: AbstractBasicClassifier): ...

def log(msg: str): ...

def best_subgroup(
        classifier: AbstractBasicClassifier,
        max_width: int,
        only_exact: bool,
        use_actions: bool,
        algo_name: str,
        oi_algo_name: str,
        bit_range: Optional[BitRange],
): ...

def min_bmgr1_w_expansions(classifier: AbstractBasicClassifier, max_expanded_bits: int): ...

def min_bmgr1_w_oi(
        classifier: AbstractBasicClassifier,
        use_actions: bool,
        reduce_width_before_lpm: bool,
        max_width: int,
        algo_name: str,
        greedy_strategy: str,
) -> Tuple[Sequence[int], Sequence[BitChain]]: ...

def incremental_updates(
        classifier: AbstractBasicClassifier,
        lpm_groups: List[AbstractBasicClassifier],
        traditional: AbstractBasicClassifier,
        tcam_size: int,
): ...
