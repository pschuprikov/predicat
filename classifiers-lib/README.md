# Optimization library for packet classifiers

## Dependencies

 * Python 3.8.8
 * GCC 10.0
 * Boost 1.69.0
 * CMake 3.18.2
 * spdlog 1.7.0
 * TBB 2019_U9
 * Catch2 2.12.3

## Build

```bash
cd p4t_native
mkdir cmake-build-release
cd cmake-build-release
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```
