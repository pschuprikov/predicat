from __future__ import annotations

from enum import Enum, auto
from typing import List, NamedTuple, Sequence, Tuple

from cls.vmrs.filter import Filter


class RangeField(NamedTuple):
    start: int
    end: int
    num_bits: int


class Direction(Enum):
    LEFT = auto()
    RIGHT = auto()


class RangeExpansionMethod(Enum):
    SRGE = auto()


class BRGC(NamedTuple):
    bits: Sequence[bool] = ()

    def to_higher(self) -> BRGC:
        return BRGC((True, *self.bits))

    def to_lower(self) -> BRGC:
        return BRGC((False, *self.bits))

    def lca(self, other: BRGC) -> BRGCNode:
        first_diff = next(
            (i
             for i, (a, b) in enumerate(zip(self.bits, other.bits))
             if a != b),
            len(self.bits)
        )
        return BRGCNode(self.bits[:first_diff], len(self.bits))

    @staticmethod
    def from_int(value: int, num_bits: int) -> BRGC:
        if num_bits == 0:
            return BRGC()
        middle = 2**(num_bits - 1)

        if value >= middle:
            return BRGC.from_int(
                2**num_bits - value - 1, num_bits - 1
            ).to_higher()
        return BRGC.from_int(value, num_bits - 1).to_lower()

    def __int__(self) -> int:
        if len(self.bits) == 0:
            return 0

        if not self.bits[0]:
            return int(BRGC(self.bits[1:]))

        return 2**len(self.bits) - 1 - int(BRGC(self.bits[1:]))

    def mirror(self, mirror: BRGCNode) -> BRGC:
        mirror_pos = len(mirror.bits)
        return BRGC((
            *self.bits[:mirror_pos],
            not self.bits[mirror_pos],
            *self.bits[mirror_pos + 1:]
        ))

    def as_node(self) -> BRGCNode:
        return BRGCNode(self.bits, len(self.bits))

    def next(self) -> BRGC:
        node = self.as_node()
        while node == node.parent.right:
            node = node.parent
        return node.parent.right.get_leftmost_leaf()

    def prev(self) -> BRGC:
        node = self.as_node()
        while node == node.parent.left:
            node = node.parent
        return node.parent.left.get_rightmost_leaf()


class BRGCNode(NamedTuple):
    bits: Sequence[bool]
    width: int

    def get_rightmost_leaf(self) -> BRGC:
        result = self
        while not result.is_leaf:
            result = result.right
        return result.to_brgc()

    def get_leftmost_leaf(self) -> BRGC:
        result = self
        while not result.is_leaf:
            result = result.left
        return result.to_brgc()

    @property
    def _is_reversed(self) -> bool:
        return sum(self.bits) % 2 != 0

    @property
    def left(self) -> BRGCNode:
        if self.is_leaf:
            raise ValueError
        return BRGCNode((*self.bits, self._is_reversed), self.width)

    @property
    def right(self) -> BRGCNode:
        if self.is_leaf:
            raise ValueError
        return BRGCNode((*self.bits, not self._is_reversed), self.width)

    @property
    def parent(self) -> BRGCNode:
        if len(self.bits) == 0:
            raise ValueError
        return BRGCNode(self.bits[:-1], self.width)

    @property
    def is_leaf(self) -> bool:
        return len(self.bits) == self.width

    def to_brgc(self) -> BRGC:
        if not self.is_leaf:
            raise ValueError
        return BRGC(self.bits)


def expand(
        field: RangeField,
        method: RangeExpansionMethod
) -> Sequence[Filter]:
    if method is RangeExpansionMethod.SRGE:
        return expand_srge(field)


def expand_srge(field: RangeField) -> List[Filter]:
    start = BRGC.from_int(field.start, field.num_bits)
    end = BRGC.from_int(field.end, field.num_bits)

    if start == end:
        return [brgc_node_to_filter(start.as_node())]

    m_left, lca, m_right = split_at_lca(start, end)

    if get_interval_size(start, m_left) <= get_interval_size(m_right, end):
        prefixes = cover_with_mirror(start, m_left, lca)

        if start.mirror(lca) == end:
            return prefixes

        new_start = start.mirror(lca).next()
        return prefixes + expand_overflowing(new_start, end, Direction.LEFT)

    prefixes = cover_with_mirror(m_right, end, lca)

    if end.mirror(lca) == start:
        return prefixes

    new_end = end.mirror(lca).prev()
    return prefixes + expand_overflowing(start, new_end, Direction.RIGHT)


def expand_overflowing(
        start: BRGC, end: BRGC, overflow_direction: Direction
) -> List[Filter]:
    m_left, lca, m_right = split_at_lca(start, end)

    if get_interval_size(start, m_left) <= get_interval_size(m_right, end):
        if overflow_direction == Direction.LEFT:
            return cover_with_mirror(m_right, end, lca)
        return prefix_cover(start, m_left) + [brgc_node_to_filter(lca.right)]

    if overflow_direction == Direction.RIGHT:
        return cover_with_mirror(start, m_left, lca)
    return prefix_cover(m_right, end) + [brgc_node_to_filter(lca.left)]


def split_at_lca(start: BRGC, end: BRGC) -> Tuple[BRGC, BRGCNode, BRGC]:
    lca = start.lca(end)
    return lca.left.get_rightmost_leaf(), lca, lca.right.get_leftmost_leaf()


def node_cover(start: BRGC, end: BRGC) -> List[BRGCNode]:
    lca = start.lca(end)
    if start == lca.get_leftmost_leaf() and end == lca.get_rightmost_leaf():
        return [lca]
    left_cover = node_cover(start, lca.left.get_rightmost_leaf())
    right_cover = node_cover(lca.right.get_leftmost_leaf(), end)
    return left_cover + right_cover


def cover_with_mirror(
        start: BRGC, end: BRGC, mirror: BRGCNode
) -> List[Filter]:
    prefixes = prefix_cover(start, end)
    for prefix in prefixes:
        prefix.mask[len(mirror.bits)] = False
    return prefixes


def prefix_cover(start: BRGC, end: BRGC) -> List[Filter]:
    return [brgc_node_to_filter(x) for x in node_cover(start, end)]


def brgc_node_to_filter(node: BRGCNode) -> Filter:
    return Filter(
        value=list(node.bits) + [True] * (node.width - len(node.bits)),
        mask=[True] * len(node.bits) + [False] * (node.width - len(node.bits)),
    )


def get_interval_size(start: BRGC, end: BRGC):
    return max(0, int(end) - int(start) + 1)
