from collections import namedtuple
from enum import Enum, auto
from functools import reduce
from itertools import chain, product
from typing import List, NamedTuple, Optional, Tuple, Sequence, Set, Any

import p4t_native

from cls.vmrs.simple import SimpleVMREntry
from cls.classifiers.abstract import AbstractBasicClassifier


class OIAlgorithm(Enum):
    ICNP_OI = auto()
    ICNP_BLOCKERS = auto()
    MIN_SIMILARITY = auto()


class MaxOIAlgorithm(Enum):
    TOP_DOWN = auto()
    MIN_DEGREE = auto()


class LPMOIAlgorithm(Enum):
    GREEDY_ALL = auto()
    ORIGINAL_SUPPORTS = auto()
    INCREMENTAL = auto()


class GreedyStrategy(Enum):
    MAX_LPM = auto()
    MAX_OI = auto()
    MAX_STEP = auto()


class BitRange(NamedTuple):
    start_bit: int
    end_bit: int


Support = Set[int]


BitChain = Sequence[Support]


def restrict_to(vmr_entry: SimpleVMREntry, bits: Support) -> SimpleVMREntry:
    bits = set(bits)
    mask = []
    value = []

    for i, (v, m) in enumerate(zip(vmr_entry.value, vmr_entry.mask)):  # pylint: disable=invalid-name
        mask.append(m and (i in bits))
        value.append(v and (i in bits))

    return SimpleVMREntry(
            tuple(value), tuple(mask), vmr_entry.action, vmr_entry.priority)


def get_entry_support(vmr_entry: SimpleVMREntry) -> Support:
    return set(i for i, m in enumerate(vmr_entry.mask) if m)


def expand(vmr_entry, bits):
    """ Performs an expansion of set of bits in the given entry.

    Args:
        vmr_entry: VMR entry.
        bits: Bit indices where exact match is required

    Returns:
        The list of expanded entries
    """
    bits = set(bits)
    mask = []
    value_options = []
    for i, (v, m) in enumerate(zip(vmr_entry.value, vmr_entry.mask)):  # pylint: disable=invalid-name
        mask.append(m or (i in bits))
        value_options.append([v] if m or (i not in bits) else [True, False])
    return [SimpleVMREntry(tuple(value), mask, vmr_entry.action, vmr_entry.priority)
            for value in product(*value_options)]


def ignore_for_chain(
        vmr_entry: SimpleVMREntry, bitchain: BitChain
) -> SimpleVMREntry:
    cur_entry = vmr_entry
    support = None
    for support in bitchain:
        cur_entry = restrict_to(cur_entry, support)
        if get_entry_support(cur_entry) == support:
            return vmr_entry
    print(bitchain, support, vmr_entry, get_entry_support(vmr_entry))
    raise AssertionError("can't restrict to chain")


def _chain2diffs(bitchain: BitChain) -> Sequence[Sequence[int]]:
    """ Returns adjacent differences of a given bit indices chain."""
    diffs = []
    last: Set[int] = set()
    for support in (set(bits) for bits in bitchain):
        diffs.append(sorted(list(support - last)))
        last = support
    return diffs


def _chain2bits(bitchain: BitChain, bitwidth) -> Sequence[int]:
    """ Returns the sequence of bit indices corresponding to a given chain."""
    return tuple(chain(*_chain2diffs(bitchain + [list(range(bitwidth))])))


def minimize_num_groups(classifier):
    """ Splits the classifier into the minimal possible number of LPM classifiers.

    Args:
        classifier: Original classifier.

    Returns:
        The list of LPM classifiers equivalent to the original.
    """
    partition, partition_indices = p4t_native.min_pmgr(classifier)

    subclassifiers = []
    for bitchain, indices in zip(partition, partition_indices):
        subclassifiers.append(classifier.subset(indices).reorder(
            _chain2bits(bitchain, classifier.bit_width), match_type='lpm'))

    return subclassifiers


def maximize_coverage_bounded(
        classifiers: Sequence[AbstractBasicClassifier],
        max_num_groups: int
):
    """ Maximize the number of rules covered by the limited number of LPM groups.

    Args:
        classifiers: The set of original classifiers.
        max_num_groups: The maximal number of groups.

    Returns:
        The pair of LPM subclassifiers list and the leftover subclassifiers.
    """

    p4t_native.log(
        f"LPM coverage maximization has started, max_groups: {max_num_groups}"
    )

    n_partitions, n_partition_indices = p4t_native.min_bmgr(
        classifiers, max_num_groups)

    subclassifiers = []
    traditionals = []
    for classifier, (partition, partition_indices) in zip(classifiers, zip(n_partitions, n_partition_indices)):
        remaining_indices = set(range(len(classifier)))

        for bitchain, indices in zip(partition, partition_indices):
            subclassifiers.append(classifier.subset(indices).reorder(
                _chain2bits(bitchain, classifier.bit_width)))
            remaining_indices -= set(indices)

        traditionals.append(classifier.subset(remaining_indices))

    return subclassifiers, traditionals


LPMGroupInfo = namedtuple('LPMGroupInfo',
                          ['classifier', 'nexp_classifier', 'indices']
                          )


def _validate_max_candidate_groups(
        max_num_groups: int,
        max_candidate_groups: Optional[int]
) -> int:
    assert (max_candidate_groups is None
            or max_num_groups <= max_candidate_groups)
    return max_candidate_groups or max_num_groups


def maximize_lpm_w_oi(
    classifier: AbstractBasicClassifier,
    max_num_groups: Optional[int],
    *,
    max_candidate_groups: Optional[int] = None,
    max_width: Optional[int] = None,
    algo: LPMOIAlgorithm = LPMOIAlgorithm.GREEDY_ALL,
    greedy_strategy: Optional[GreedyStrategy] = GreedyStrategy.MAX_LPM,
    use_actions: bool = False,
    reduce_width_before_lpm: bool = True,
) -> Tuple[List[AbstractBasicClassifier], AbstractBasicClassifier]:
    assert greedy_strategy is None or algo is LPMOIAlgorithm.GREEDY_ALL

    max_candidate_groups = _validate_max_candidate_groups(
        max_num_groups, max_candidate_groups)

    indices = list(range(len(classifier)))
    lpm_groups: List[LPMGroupInfo] = []

    while not _is_enough_groups(lpm_groups, max_candidate_groups) and len(indices) > 0:
        p4t_native.log(f"LPM has started for group #{len(lpm_groups) + 1}")

        lpm_indices: List[int]
        bitchain: BitChain
        lpm_indices, bitchain = p4t_native.min_bmgr1_w_oi(
            classifier.subset(indices),
            use_actions,
            reduce_width_before_lpm,
            max_width,
            algo.name.lower(),
            greedy_strategy.name.lower() if greedy_strategy else None)
        current_indices = [indices[i] for i in lpm_indices]
        # TODO hack
        bitchain = [set(x) for x in bitchain]

        group = classifier.subset([])
        for i in current_indices:
            group.vmr.append(ignore_for_chain(classifier[i], bitchain))

        lpm_groups.append(LPMGroupInfo(
            classifier=group,
            nexp_classifier=None,
            indices=current_indices,
        ))

        indices = sorted(set(indices) - set(current_indices))

    selected_groups = lpm_groups
    rest_indices = sorted(set(range(len(classifier))) - reduce(
        lambda x, y: x | y,
        (set(x.indices) for x in selected_groups),
        set()
    ))

    return (
        [x.classifier for x in selected_groups],
        classifier.subset(rest_indices)
    )


def minimize_oi_lpm(classifier, max_width, algo: OIAlgorithm, max_num_groups,
                    *,
                    max_expanded_bits=None, provide_non_expanded=False,
                    max_candidate_groups=None, use_actions=False,
                    max_oi_algo: MaxOIAlgorithm = MaxOIAlgorithm.TOP_DOWN):
    """ Minimizes the number of subclassifiers, which are both LPM and OI.

    Args:
        classifier: Initial classifier.
        max_width: Maximal allowed classification width in the resulting subclassifiers.
        algo: OI algorithm to use
        max_num_groups: Maximal allowed number of subclassifiers.
        max_expanded_bits: Maximal allowed number of expanded bits.
        provide_non_expanded: Whether non-expanded versions should be returned.
        max_candidate_groups: The total number of candidate groups.
        max_oi_algo: The algorithm used for maximal OI.

    Returns:
        Pair of subclassifiers list and classifier with leftover rules. If
        provide_non_expanded is True, then the third element in a tuple  is
        a list of non-expanded classifiers.
    """
    assert max_expanded_bits is not None or not provide_non_expanded

    max_candidate_groups = _validate_max_candidate_groups(
        max_num_groups, max_candidate_groups)

    indices = list(range(len(classifier)))
    lpm_groups: List[LPMGroupInfo] = []

    while not _is_enough_groups(lpm_groups, max_candidate_groups) and len(indices) > 0:
        p4t_native.log(f"OI-LPM has started for group #{len(lpm_groups) + 1}")

        oi_bits, oi_indices = p4t_native.best_subgroup(
            classifier.subset(indices), max_width, False, use_actions,
            algo.name.lower(), max_oi_algo.name.lower(), None
        )
        oi_classifier = classifier.subset(
            indices[i] for i in oi_indices
        ).reorder(oi_bits)

        if max_expanded_bits is None:
            [[bitchain]], [[lpm_indices]] = p4t_native.min_bmgr(
                [oi_classifier], 1
            )
            group = oi_classifier.subset(lpm_indices).reorder(
                _chain2bits(bitchain, oi_classifier.bit_width)
            )
            nexp_group = None
        else:
            bitchain, lpm_indices, expansions = p4t_native.min_bmgr1_w_expansions(
                oi_classifier, max_expanded_bits)

            group = oi_classifier.subset([])
            for i, exp in zip(lpm_indices, expansions):
                for entry in expand(oi_classifier[i], exp):
                    group.vmr.append(entry)

            if provide_non_expanded:
                nexp_group = oi_classifier.subset(lpm_indices).reorder(
                    _chain2bits(bitchain, oi_classifier.bit_width)
                )
            else:
                nexp_group = None

        current_indices = [indices[oi_indices[i]] for i in lpm_indices]
        lpm_groups.append(LPMGroupInfo(
            classifier=group,
            nexp_classifier=nexp_group,
            indices=current_indices
        ))

        indices = sorted(set(indices) - set(current_indices))

        p4t_native.log("OI decomposition has finished")

    lpm_groups.sort(key=lambda x: len(x.indices), reverse=True)
    selected_groups = lpm_groups[:max_num_groups]
    rest_indices = sorted(set(range(len(classifier))) - reduce(
        lambda x, y: x | y,
        (set(x.indices) for x in selected_groups),
        set()
    ))

    if provide_non_expanded:
        return (
            [x.classifier for x in selected_groups],
            classifier.subset(rest_indices),
            [x.nexp_classifier for x in selected_groups]
        )
    else:
        return (
            [x.classifier for x in selected_groups],
            classifier.subset(rest_indices)
        )


OIGroupInfo = namedtuple('OIGroupInfo', ['classifier', 'indices'])


def decompose_oi(classifier,
                 max_width,
                 algo: OIAlgorithm,
                 max_num_groups: Optional[int],
                 *,
                 only_exact=False,
                 use_actions: bool = False,
                 max_candidate_groups=None,
                 max_oi_algo: MaxOIAlgorithm = MaxOIAlgorithm.TOP_DOWN,
                 bit_range: Optional[BitRange] = None):
    """ Decomposes a classifier into a set of order-independent subclassifiers.

    Args:
        classifier: Initial classifier.
        max_width: Maximal allowed classification width in the resulting subclassifiers.
        algo: Algorithm to use
        only_exact: Whether only exact bits should be allowed (False by default).
        max_num_groups: Maximal allowed number of subclassifiers.
        max_candidate_groups: The total number of candidate groups.
        max_oi_algo: The algorithm used for maximal OI.
        bit_range: The range of bits that can be used for classification.

    Returns:
        Pair of subclassifiers list and classifier with leftover rules.
    """

    assert (max_candidate_groups is None
            or max_num_groups <= max_candidate_groups)
    if max_candidate_groups is None:
        max_candidate_groups = max_num_groups

    p4t_native.log(
        f"OI decomposition has started: exact={only_exact}"
        f", bitrange={bit_range}")

    indices = list(range(len(classifier)))
    oi_groups: List[OIGroupInfo] = []
    while not _is_enough_groups(oi_groups, max_candidate_groups) and len(indices) > 0:
        oi_bits, oi_indices = p4t_native.best_subgroup(
            classifier.subset(indices), max_width, only_exact, use_actions,
            algo.name.lower(), max_oi_algo.name.lower(), bit_range)

        current_indices = [indices[i] for i in oi_indices]
        oi_groups.append(OIGroupInfo(
            classifier=classifier.subset(oi_indices).reorder(oi_bits),
            indices=current_indices
        ))
        indices = sorted(set(indices) - set(current_indices))

    p4t_native.log("OI decomposition has completed")

    oi_groups.sort(key=lambda x: len(x.indices), reverse=True)
    selected_groups = oi_groups[:max_num_groups]
    rest_indices = sorted(set(range(len(classifier))) - reduce(
        lambda x, y: x | y,
        (set(x.indices) for x in selected_groups),
        set()
    ))

    return ([x.classifier for x in selected_groups],
            classifier.subset(rest_indices))


def _is_enough_groups(
        groups: Sequence[Any],
        max_candidate_groups: Optional[int]
) -> bool:
    return max_candidate_groups is not None and len(groups) >= max_candidate_groups


IncrementalBatchStats = namedtuple(
    'IncrementalBatchStats',
    ('num_in_groups', 'num_traditional')
)


def test_incremental(classifier, max_width, max_num_groups, max_traditional, *,
                     algo=OIAlgorithm.ICNP_BLOCKERS, max_oi_algo=MaxOIAlgorithm.MIN_DEGREE,
                     max_candidate_groups=None):
    """ Test incremental updates for a classifier

    Args:
        classifier: Test classifier.
        max_width: Maximal allowed classification width.
        max_num_groups: Maximal allowed number of groups.
        max_traditional: Maximal allowed size of traditional representation.
        algo: OI algorithm to use
        max_candidate_groups: The total number of candidate groups
        max_oi_algo: The algorithm used for maximal OI (Possible values
            'top_down' and 'min_degree').

    Returns:
        A list of Incremental Batch Stats
    """

    p4t_native.log("Running incremental updates...")

    incremental_stats = []
    num_added, lpm_groups, traditional = 0, [], classifier.subset([])
    while num_added < len(classifier):
        incremental, traditional = p4t_native.incremental_updates(
            classifier.subset(range(num_added, len(classifier))),
            lpm_groups, traditional, max_traditional)
        incremental_stats.append(
            IncrementalBatchStats(incremental, traditional))
        num_added += incremental + traditional
        p4t_native.log(
            f"... incremental: {incremental}, traditional: {traditional}"
            f" total: {num_added}, ... {max_traditional}")

        if num_added < len(classifier):
            lpm_groups, traditional = minimize_oi_lpm(
                classifier.subset(range(num_added)), max_width,
                algo, max_num_groups, max_oi_algo=max_oi_algo,
                max_candidate_groups=max_candidate_groups)

    return incremental_stats
