from typing import Sequence


class Filter:
    def __init__(self, value: Sequence[bool], mask: Sequence[bool]):
        self.value = list(value)
        self.mask = list(mask)

    def __add__(self, other):
        return Filter(self.value + other.value, self.mask + other.mask)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Filter):
            return False
        return all(
            m1 == m2 and (m1 and b1) == (m2 and b2)
            for (b1, m1), (b2, m2) in
            zip(zip(self.value, self.mask), zip(other.value, other.mask))
        )

    def __str__(self):
        return "".join(
            ('1' if bit else '0') if mask_bit else '*'
            for (bit, mask_bit) in zip(self.value, self.mask)
        )

    def __repr__(self):
        return str(self)

    def __len__(self):
        return len(self.value)
