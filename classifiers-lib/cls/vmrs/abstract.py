import abc
import collections.abc as collections
from typing import NamedTuple, Sequence, Any, MutableSequence


class AbstractVMREntry(NamedTuple):
    """A target independent vmr entry representation.
    """

    value: Sequence[bool]
    """The value to be matched against."""

    mask: Sequence[bool]
    """ The mask to be applied when matching."""

    action: Any
    """Action to execute on match."""

    priority: Any
    """Entry priority (if needed)."""


class AbstractVMR(collections.MutableSequence, MutableSequence[AbstractVMREntry]):
    """ This is an abstract class that defines what VMR in general are.

    Most importantly, VMR is a mutable sequence of VMR entries, where each VMR
    entry is expected to comply with :py:class:`p4t.vmrs.simple.SimpleVMR`
    interface.
    """
    @property
    @abc.abstractmethod
    def bit_width(self):
        """ VMR's bit width. """
        pass

    @property
    @abc.abstractmethod
    def default_action(self):
        """ Action to execute on no match."""
        pass

    @default_action.setter
    def default_action(self, value):
        pass

    @abc.abstractmethod
    def create_instance(self, bit_width=None, table=None):
        """ Creates new VMR instance.

        It is a factory method, table must always be supplied for Bmv2VMR.

        Args:
            bit_width: Number of classification bits.
            table: The P4 table corresponding to the new VMR.
        """
        pass
