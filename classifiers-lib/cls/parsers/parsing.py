from functools import wraps
from itertools import chain, repeat

from cls.classifiers.simple import BasicClassifier
from cls.vmrs.simple import SimpleVMR, SimpleVMREntry
from cls.vmrs.filter import Filter


def classifier_format(*widths):
    def decorator(func):
        @wraps(func)
        def wrapper(line, line_idx):
            return func(line, line_idx)
        wrapper.width = sum(widths)
        return wrapper
    return decorator


def _parse_range(range):
    return tuple(map(int, range.split(' : ')))


def _int_to_bit(x, num_bits):
    result = [int(digit) for digit in bin(x)[2:]]
    return [0] * (num_bits - len(result)) + result


def _octets_to_bits(s):
    return list(chain.from_iterable(
        _int_to_bit(int(octet), 8) for octet in s.split('.')
    ))


def _ip_to_filter(ip):
    if '/' in ip:
        ip, nm = ip.split('/')
    else:
        ip, nm = ip, '32'

    value = _octets_to_bits(ip)

    if '.' in nm:
        mask = list(1 - x for x in _octets_to_bits(nm))
    else:
        mask = list(chain(repeat(1, int(nm)), repeat(0, 32 - int(nm))))

    return Filter(value, mask)


def _maybe_exact_to_filter(x, num_bits):
    if int(x) < 0:
        return Filter(repeat(1, num_bits), repeat(0, num_bits))
    else:
        return Filter(_int_to_bit(int(x), num_bits), repeat(1, num_bits))


def _field_to_filter(proto, num_bits):
    value, mask = (_int_to_bit(int(x, 16), num_bits) for x in proto.split('/'))
    return Filter(value, mask)


def _chars_to_filter(str):
    return Filter(
        (True if c == '1' else False for c in str),
        (True if c != '*' else False for c in str)
    )


def _pylist_to_filters(lst):
    return [_chars_to_filter(s) for s in eval(lst)]


def text(bit_width):
    @classifier_format(bit_width)
    def parser(line):
        return [_chars_to_filter(line)]
    return parser


@classifier_format(32, 32, 4, 4)
def icnp(line):
    src_ip, dst_ip, _, _, x1, x2 = line.split('\t')
    return [
        _ip_to_filter(src_ip) + _ip_to_filter(dst_ip) +
        _maybe_exact_to_filter(x1, 4) + _maybe_exact_to_filter(x2, 4)
    ]


@classifier_format(32, 32)
def classbench_ips(line):
    src_ip, dst_ip, *_ = line[1:].split('\t')
    return [_ip_to_filter(src_ip) + _ip_to_filter(dst_ip)]


@classifier_format(32, 32, 8, 16)
def classbench(line):
    src_ip, dst_ip, in_port, out_port, proto, eth_type, _ = line[1:].split(
        '\t')
    return [
        _ip_to_filter(src_ip) + _ip_to_filter(dst_ip) +
        _field_to_filter(proto, 8) + _field_to_filter(eth_type, 16)
    ]


@classifier_format(32, 32, 16, 16, 8)
def classbench_expanded(line, _line_idx):
    src_ip, dst_ip, proto, in_port, out_port = line[1:].split('\t')
    return [
        _ip_to_filter(src_ip) + _ip_to_filter(dst_ip) +
        x + y + _field_to_filter(proto, 8)
        for x in _pylist_to_filters(in_port)
        for y in _pylist_to_filters(out_port)
    ]


@classifier_format(32, 32, 32)
def classbench_ips_unique(line, line_idx):
    src_ip, dst_ip, _, _, _ = line[1:].split('\t')
    return [
        _ip_to_filter(src_ip) +
        _ip_to_filter(dst_ip) +
        _maybe_exact_to_filter(line_idx, 32)
    ]


@classifier_format(32, 32, 32)
def classbench_original(line, line_idx):
    src_ip, dst_ip, _, _, _, _, _ = line[1:].split('\t')
    return [
        _ip_to_filter(src_ip) +
        _ip_to_filter(dst_ip) +
        _maybe_exact_to_filter(line_idx, 32)
    ]


def read_classifier(clsf_format, lines) -> BasicClassifier:
    vmr = SimpleVMR(clsf_format.width)
    for i, line in enumerate(lines):
        for flt in clsf_format(line, i):
            vmr.append(SimpleVMREntry(
                flt.value, flt.mask,
                action=i + 1, priority=0, length=clsf_format.width))
    return BasicClassifier(vmr)
