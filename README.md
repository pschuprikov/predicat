# PREDICAT: Efficient Packet Classification via Prefix Disjointness (Code for evaluation)

## Environment setup

1. Follow the instructions in [README.md](classifiers-lib/README.md) to build `classifiers-lib`

2. Install the following dependencies for the evaluation runner:

   - Python 3.8
   - Click 7.1.2 (python library) 
   - Bitstring 3.1.5 (python library)
   - Numpy 1.19.1 (python library)
   - Pandas 1.1.1 (python library)

3. Change dir to `testing` and set up the environment:

    ```bash
    export PYTHONPATH=$PYTHONPATH:"../classifiers-lib/:../classifiers-lib/p4t_native/cmake-build-release/p4t"
    ```

## Running evaluation

Below is the general command to run the evaluation:

```bash
python -m predicat_runner.checker [PARAMETERS] ../classbench/expanded/*.txt
```

To guide the parameter choice, please, use `--help`.

The evaluation result is going to be saved into a `data.tsv` file, which can be further analyzed by running `misc.py`

### Getting results for Figure 1 (104 bit width)

- SAX-PAC

    ```bash
    python -m predicat_runner.checker --oi-use-actions --oi-cutoff 0 optimize-oi ../classbench/expanded/*.txt
    ```

 - PREDICAT

    ```bash
     python -m predicat_runner.checker --oi-use-actions --lpm-max-candidate-groups 0  optimize-lpm-w-oi --oi-lpm-algo incremental --oi-lpm-reduce-width-after ../classbench/expanded/*.txt
    ```

 - LPM-PR

    ```bash
    # N must be found by binary search, run `misc.py` to get some guidance 
    python -m predicat_runner.checker --lpm-max-groups N --lpm-max-candidate-groups 0 optimize-lpm ../classbench/expanded/*.txt
    ```

 - EXACT

    ```bash
    python -m predicat_runner.checker --oi-only-exact --oi-use-actions --lpm-max-candidate-groups 0  optimize-oi ../classbench/expanded/*.txt  
    ```

### Getting results for Figure 2 (32 bit width)

 - SAX-PAC

    ```bash
    python -m predicat_runner.checker --oi-cutoff 0 --oi-use-actions --oi-bit-width 32 optimize-oi ../classbench/expanded/*.txt  
    ```

 - PREDICAT

    ```bash
    python -m predicat_runner.checker --oi-use-actions --lpm-max-candidate-groups 0 --oi-bit-width 32 optimize-lpm-w-oi --oi-lpm-algo incremental --oi-lpm-reduce-width-after ../classbench/expanded/*.txt
    ```

 - LPM-PR

    ```bash
    python -m predicat_runner.checker --oi-use-actions --lpm-max-candidate-groups 0 --oi-bit-width 32 optimize-oi-lpm-joint  ../classbench/expanded/*.txt
    ```

 - EXACT

    same as before, see the paper for an explanation why
