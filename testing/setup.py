from distutils.core import setup

setup(
    name="predicate-runner",
    version="0.1dev",
    packages=['predicate_runner'],
    install_requires=[
        'click', 'classifiers-lib'
    ],
    entry_points='''
        [console_scripts]
        run=predicate_runner.checker:greet
    '''
)
